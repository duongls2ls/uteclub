import Vue from 'vue'
import Router from 'vue-router'
import guards from '@/admin/guards'

Vue.use(Router);
export default new Router({
  mode: 'history',
  scrollBehavior() {
    return window.scrollTo({top: 0, behavior: 'smooth'});
  },
  routes: [

    // Login

    {
      path: '/admin/login',
      name: 'login',
      meta: {
        title: 'Login',
        layout: 'userpages'
      },
      component: () => import('../admin/Auth/Login.vue'),
      beforeEnter: guards.authenticate
    },

    // Checkpoint

    {
      path: '/admin/checkpoint',
      name: 'checkpoint',
      meta: {
        title: 'Checkpoint',
        layout: 'userpages'
      },
      component: () => import('../admin/Auth/Checkpoint.vue'),
      beforeEnter: guards.verifyOtpAtLogin
    },

    {
      path: '/admin/checkpoint/google2fa/activate',
      name: 'google2fa.activate',
      meta: {
        title: 'Activate Google Two Factor Authentication',
        layout: 'userpages'
      },
      component: () => import('../admin/Auth/Google2fa/Activate.vue'),
      beforeEnter: guards.activateGoogle2fa
    },


    // Password Reset

    {
      path: '/admin/password/reset',
      name: 'password.request',
      meta: {
        title: 'Reset Password',
        layout: 'userpages'
      },
      component: () => import('../admin/Auth/Passwords/Email.vue'),
      beforeEnter: guards.authenticate
    },

    {
      path: '/admin/password/reset/:token',
      name: 'password.reset',
      meta: {
        title: 'Reset Password',
        layout: 'userpages'
      },
      component: () => import('../admin/Auth/Passwords/Reset.vue'),
      props: true,
      beforeEnter: guards.authenticate
    },

    // Dashboard

    {
      path: '/admin/dashboard',
      name: 'dashboard',
      meta: {
        title: 'Dashboard',
        layout: 'defaultadmin'
      },
      component: () => import('../admin/Dashboard/Dashboard.vue'),
      beforeEnter: guards.accessApp
    },
    
    // Users

    {
      path: '/admin/users',
      name: 'users',
      meta: {
        title: 'Users',
        layout: 'defaultadmin'
      },
      component: () => import('../admin/Users/Users.vue'),
      beforeEnter: guards.accessApp
    },
    // Clients

    {
      path: '/admin/clients',
      name: 'clients',
      meta: {
        title: 'Clients',
        layout: 'defaultadmin'
      },
      component: () => import('../admin/Clients/Clients.vue'),
      beforeEnter: guards.accessApp
    },
    //Clubs
    {
      path: '/admin/clubs',
      name: 'clubs',
      meta: {
        title: 'Clubs',
        layout: 'defaultadmin'
      },
      component: () => import('../admin/Clubs/Clubs.vue'),
      beforeEnter: guards.accessApp
    },
    //Hoạt động Admin
    {
      path: '/admin/activaty',
      name: 'activaty',
      meta: {
        title: 'Hoạt động',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/Activaty/Activaties.vue'),
      beforeEnter: guards.accessApp
    },
    {
      path: '/admin/activaty/attendance/:idAct',
      name: 'attendance',
      meta: {
        title: 'Điểm danh',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/Activaty/Attendance.vue'),
      beforeEnter: guards.accessApp
    },
    //PageConnect Admin
    {
      path: '/admin/pageconnect',
      name: 'pageconnect',
      meta: {
        title: 'Trang kết nối',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/PageConnect/PageConnect.vue'),
      beforeEnter: guards.accessApp
    },
    {
      path: '/admin/reportactivaty',
      name: 'reportactivaty',
      meta: {
        title: 'Trang thống kê',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/ReportActivaty/ReportActivaty.vue'),
      beforeEnter: guards.accessApp
    },
    //Thành viên CLB
    {
      path: '/admin/userclb/user',
      name: 'userclb',
      meta: {
        title: 'Thành viên Câu Lạc Bộ',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/User_CLB/UserCLB.vue'),
      beforeEnter: guards.accessApp
    },
    {
      path: '/admin/userclb/duyet',
      name: 'duyet',
      meta: {
        title: 'Danh sách thành viên đăng ký',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/User_CLB/DuyetUser.vue'),
      beforeEnter: guards.accessApp
    },
    //PostCLB_Admin
    {
      path: '/admin/post/duyet',
      name: 'duyetpostclb',
      meta: {
        title: 'Duyệt bài đăng',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/Post/DuyetPost.vue'),
      beforeEnter: guards.accessApp
    },
    {
      path: '/admin/post/list',
      name: 'postadmin',
      meta: {
        title: 'Danh sách bài đăng',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/Post/PostCLB.vue'),
      beforeEnter: guards.accessApp
    },
    {
      path: '/admin/comment/list/:id',
      name: 'commentadmin',
      meta: {
        title: 'Danh sách bình luận',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/Post/Comment.vue'),
      beforeEnter: guards.accessApp
    },
    //Thu tiền
    {
      path: '/admin/costactivity',
      name: 'costactivity',
      meta: {
        title: 'Thu tiền',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/CostActivity/CostActivity.vue'),
      beforeEnter: guards.accessApp
    },
    //Thu tiền
    {
      path: '/admin/costactivity/attendance/:idCost',
      name: 'attendance_Cost',
      meta: {
        title: 'Điểm danh thu tiền',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/CostActivity/Attendance.vue'),
      beforeEnter: guards.accessApp
    },
    //Chi tiền
    {
      path: '/admin/expenses',
      name: 'expenses',
      meta: {
        title: 'Chi tiền',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/Expenses/Expenses.vue'),
      beforeEnter: guards.accessApp
    },
    {
      path: '/admin/detailexpenses/:idEx',
      name: 'detailexpenses',
      meta: {
        title: 'Chi tiết chi tiền',
        layout: 'defaultadmin'
      },
      component: () => import('@/admin/Expenses/DetailExpenses.vue'),
      beforeEnter: guards.accessApp
    },
    // Clients

    {
      path: '/',
      name: 'Home',
      meta: {
        title: 'Home', 
        layout: 'defaultclientsite'
      },
      component: () => import('../Clients/Views/Home.vue'),    
    },
    {
      path: '/listactivaty',
      name: 'ListActivaty',
      meta: {
        title: 'ListActivaty', 
        layout: 'defaultclient'
      },
      component: () => import('../Clients/Views/Activaty/ListActivaty'),    
    },
    {
      path: '/listactivatyon',
      name: 'ListActivatyOn',
      meta: {
        title: 'ListActivatyOn', 
        layout: 'defaultclient'
      },
      component: () => import('../Clients/Views/Activaty/ListActivatyOn'),    
    },
    {
      path: '/detailactivaty/:idAvt',
      name: 'DetailActivaty',
      meta: {
        title: 'DetailActivaty', 
        layout: 'defaultclientsite'
      },
      component: () => import('../Clients/Views/Activaty/DetailActivaty'),    
    },
    {
      path: '/detaiactivityclub/:slug',
      name: 'DetaiActivityClub',
      meta: {
        title: 'DetaiActivityClub',
        layout: 'defaultclientsite'
      },
      component: () => import('../Clients/Views/Activaty/DetaiActivityClub'),
    },
    {
      path: '/detailpost',
      name: 'DetailPost',
      meta: {
        title: 'DetailPost',
        layout: 'defaultclientsite'
      },
      component: () => import('../Clients/Views/Post/DetailPost'),
    },
    {
      path: '/listclub',
      name: 'ListClub',
      meta: {
        title: 'ListClub',
        layout: 'defaultclient'
      },
      component: () => import('../Clients/Views/Club/ListClub'),
    },
    {
      path: '/detailclub/:id',
      name: 'DetailClub',
      meta: {
        title: 'DetailClub',
        layout: 'defaultclient'
      },
      component: () => import('../Clients/Views/Club/DetailClub'),
    },
    {
      path: '/listmembership',
      name: 'ListMembership',
      meta: {
        title: 'ListMembership',
        layout: 'defaultclient'
      },
      component: () => import('../Clients/Views/Club/ListMembership'),
    },
    {
      path: '/listactivity',
      name: 'ListActivity',
      meta: {
        title: 'ListActivity',
        layout: 'defaultclient'
      },
      component: () => import('../Clients/Views/Club/ListActivity'),
    },
    {
      path: '/feesuser',
      name: 'FeesUser',
      meta: {
        title: 'FeesUser',
        layout: 'defaultclient'
      },
      component: () => import('../Clients/Views/User/FeesUser'),
    },    
    {
      path: '/profile',
      name: 'Profile',
      meta: {
        title: 'Profile',
        layout: 'defaultclient'
      },
      component: () => import('../Clients/Views/User/ProfileUser'),
    },
    {
      path: '/editprofile',
      name: 'EditProfile',
      meta: {
        title: 'EditProfile',
        layout: 'defaultclient'
      },
      component: () => import('../Clients/Views/User/EditProfile'),
    },   
    // User Settings
    {
      path: '/admin/settings/user',
      name: 'settings.user',
      meta: {
        title: 'User Settings',
        layout: 'defaultadmin'
      },
      component: () => import('../admin/Settings/User/User.vue'),
      beforeEnter: guards.accessApp
    },
    // Clubs Settings
    {
      path: '/admin/settings/clb',
      name: 'settings.club',
      meta: {
        title: 'Thông tin câu lạc bộ',
        layout: 'defaultadmin'
      },
      component: () => import('../admin/Settings/CLB/Club.vue'),
      beforeEnter: guards.accessApp
    },
    // Pages

    {
      path: '/admin/pages/login-boxed',
      name: 'login-boxed',
      meta: {layout: 'userpages'},
      component: () => import('../admin/DemoPages/UserPages/LoginBoxed.vue'),
    },
    {
      path: '/admin/pages/register-boxed',
      name: 'register-boxed',
      meta: {layout: 'userpages'},
      component: () => import('../admin/DemoPages/UserPages/RegisterBoxed.vue'),
    },
    {
      path: '/admin/pages/forgot-password-boxed',
      name: 'forgot-password-boxed',
      meta: {layout: 'userpages'},
      component: () => import('../admin/DemoPages/UserPages/ForgotPasswordBoxed.vue'),
    },

    // Elements

    {
      path: '/admin/elements/buttons-standard',
      name: 'buttons-standard',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Elements/Buttons/Standard.vue'),
    },
    {
      path: '/admin/elements/dropdowns',
      name: 'dropdowns',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Elements/Dropdowns.vue'),
    },
    {
      path: '/admin/elements/icons',
      name: 'icons',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Elements/Icons.vue'),
    },
    {
      path: '/admin/elements/badges-labels',
      name: 'badges',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Elements/Badges.vue'),
    },
    {
      path: '/admin/elements/cards',
      name: 'cards',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Elements/Cards.vue'),
    },
    {
      path: '/admin/elements/list-group',
      name: 'list-group',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Elements/ListGroups.vue'),
    },
    {
      path: '/admin/elements/timelines',
      name: 'timeline',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Elements/Timeline.vue'),
    },
    {
      path: '/admin/elements/utilities',
      name: 'utilities',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Elements/Utilities.vue'),
    },

    // Components

    {
      path: '/admin/components/tabs',
      name: 'tabs',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Components/Tabs.vue'),
    },
    {
      path: '/admin/components/accordions',
      name: 'accordions',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Components/Accordions.vue'),
    },
    {
      path: '/admin/components/modals',
      name: 'modals',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Components/Modals.vue'),
    },
    {
      path: '/admin/components/progress-bar',
      name: 'progress-bar',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Components/ProgressBar.vue'),
    },
    {
      path: '/admin/components/tooltips-popovers',
      name: 'tooltips-popovers',
      meta: {layout: 'default'},
      component: () => import('../admin/DemoPages/Components/TooltipsPopovers.vue'),
    },
    {
      path: '/admin/components/carousel',
      name: 'carousel',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Components/Carousel.vue'),
    },
    {
      path: '/admin/components/pagination',
      name: 'pagination',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Components/Pagination.vue'),
    },
    {
      path: '/admin/components/maps',
      name: 'maps',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Components/Maps.vue'),
    },

    // Tables

    {
      path: '/admin/tables/regular-tables',
      name: 'regular-tables',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Tables/RegularTables.vue'),
    },

    // Dashboard Widgets

    {
      path: '/admin/widgets/chart-boxes-3',
      name: 'chart-boxes-3',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Widgets/ChartBoxes3.vue'),
    },

    // Forms

    {
      path: '/admin/forms/controls',
      name: 'controls',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Forms/Elements/Controls.vue'),
    },
    {
      path: '/admin/forms/layouts',
      name: 'layouts',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Forms/Elements/Layouts.vue'),
    },
    // Charts

    {
      path: '/admin/charts/chartjs',
      name: 'chartjs',
      meta: {layout: 'defaultadmin'},
      component: () => import('../admin/DemoPages/Charts/Chartjs.vue'),
    },
  ]
})
