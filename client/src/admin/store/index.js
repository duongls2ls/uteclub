import Vue from 'vue'
import Vuex from 'vuex'

import login from './modules/auth/login'
import checkpoint from './modules/auth/checkpoint'
import activaty from './client/activaty/activaty'
import listactivaty from './client/activaty/listactivaty'
import club from './client/club/club'
import listclub from './client/club/listclub'
import post from './client/post/post'
import comment from './client/post/comment'
import connectpage from './client/connectpage/connectlink'
import user from './client/user/user'
import like from './client/post/like'
//admin
import listactivaty_ad from '@/admin/store/admin/activaty/listactivaty_ad'
import activaty_ad from '@/admin/store/admin/activaty/activaty_ad'
import usernotclb_ad from '@/admin/store/admin/clubs/usernotclb_ad'
import report_activaty from '@/admin/store/admin/report/report_activaty'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    login,
    checkpoint,
    activaty,
    listactivaty,
    club,
    listclub,
    post,
    listactivaty_ad,
    activaty_ad,
    connectpage,
    comment,
    usernotclb_ad,
    user,
    like,
    report_activaty
  }
})