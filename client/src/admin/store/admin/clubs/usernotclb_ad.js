import axios from 'axios'
const usernotclb_adModules={
    state:{
        user_not_clb_ad:[]
    },
    getters:{
        user_not_clbs_ad:state=>state.user_not_clb_ad,
        
    },
    actions:{
        async getUsernotCLB_Ad({commit}){
            try {
                const res=await axios.get(`http://127.0.0.1:8000/api/user/getusernotclub`)
                commit('SET_USERNOTCLB_AD',res.data)
            } catch (error) {
                console.log(error)
            }
        }
    },
    mutations:{
        SET_USERNOTCLB_AD(state,userdata){
            state.user_not_clb_ad=userdata
        },
    },
    
}
export default usernotclb_adModules