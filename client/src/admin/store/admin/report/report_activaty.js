import axios from 'axios'
const report_adModules={
    state:{
        list_report_ad:[],
        list_report_clone:[]
    },
    getters:{
        list_reports_ad:state=>state.list_report_ad
    },
    actions:{
        async getListReport({commit}){
            try {
                const res=await axios.get(`/api/activitie/reportact`)
                commit('SET_LISTREPORT_AD',res.data.data)
            } catch (error) {
                console.log(error)
            }
        },
    },
    mutations:{
        SET_LISTREPORT_AD(state,list_report){
            state.list_report_ad=list_report
            state.list_report_clone=list_report
        },
        
        SET_FILTER_REPORT(state,idCLB){
            if(idCLB==0){
                state.list_report_ad=state.list_report_clone
            }
            else
            {
                state.list_report_ad=state.list_report_clone.filter(item=>item.club_id==idCLB)
            }
        },
        SET_FILTER_SEARCH_REPORT(state,filter){
            if(filter.idCLB==0 && filter.key_search==""){
                state.list_report_ad=state.list_report_clone
            }else if(filter.idCLB!=0 && filter.key_search==""){
                state.list_report_ad=state.list_report_clone.filter(item=>item.club_id==filter.idCLB)
            }else if(filter.idCLB==0 && filter.key_search!=""){
                state.list_report_ad=state.list_report_clone.filter(item=>item.name.search(filter.key_search)!=-1)
            }else {
                state.list_report_ad=state.list_report_clone.filter(item=>item.name.search(filter.key_search)!=-1 && item.club_id==filter.idCLB)
            }
        }
    },
    
}
export default report_adModules;
