import axios from 'axios'
const listactivaty_adModules={
    state:{
        listactivaty_ad:[],
        idClubs_ad:'',
        nameClubs_ad:''
    },
    getters:{
        listactivaties_ad:state=>state.listactivaty_ad,
        idClubs_ad:state=>state.idClubs_ad,
        nameClubs_ad:state=>state.nameClubs_ad
    },
    actions:{
        async getListActivaty_Ad({commit},idUser){
            try {
                const res=await axios.get(`http://127.0.0.1:8000/api/club/getallclub`)
                commit('SET_LISTACTIVATY_AD',res.data.data)
                commit('GET_IDCLUBS_AD',idUser)
            } catch (error) {
                console.log(error)
            }
        }
    },
    mutations:{
        SET_LISTACTIVATY_AD(state,listdata){
            state.listactivaty_ad=listdata
        },
        GET_IDCLUBS_AD(state,idUser){
            state.listactivaty_ad.map(club=>{
                if(club.user_id==idUser){
                    state.idClubs_ad=club.id
                    state.nameClubs_ad=club.name
                }
            })
        }
    },
    
}
export default listactivaty_adModules