import axios from 'axios'
const connectpageModules={
    state:{
        linkpage: []
    },
    getters:{
        linkpage:state=>state.linkpage,         
    },
    actions:{
        async getLinkPage({commit}){
            try {
                const res=await axios.get(`http://127.0.0.1:8000/api/pageconnects`)
                commit('SET_LINKCONNECT',res.data)
            } catch (error) {
                console.log(error)
            }
        },       
    },
    mutations:{
        SET_LINKCONNECT(state,linkpage){
            state.linkpage=linkpage
        },      
    },    
}
export default connectpageModules