import axios from 'axios'
const postModules={
    state:{
        post:[],
        getpostfilter: [],
        getpostfilterclub: [],
        getpostuser:[]
    },
    getters:{
        post:state=>state.post,
        getpostfilter:state=>state.getpostfilter,
        getpostfilterclub:state=>state.getpostfilterclub,
        getpostuser:state=>state.getpostuser
    },
    actions:{
        async getPost({commit}){
            try {
                const res=await axios.get(`/api/posts`)   
                commit('SET_POSTS',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getPostlogin({commit},id){
            try {
                const res=await axios.get(`/api/postlike/${id}`)   
                commit('SET_POSTS',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getpostid({commit},id){
            try {                
                const res=await axios.get(`/api/posts/${id}`)   
                commit('SET_POSTID',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getpostidclub({commit},id){
            try {                
                const res=await axios.get(`/api/postclub/${id}`)                
                commit('SET_POSTIDCLUBS',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getpostidclubid({commit},data){
            try {                
                const res=await axios.get(`/api/postclublike/${data.user}/${data.club}`)                
                commit('SET_POSTIDCLUBS',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getpostiduserid({commit},data){
            try {                
                const res=await axios.get(`/api/postuser/${data}`)
                commit('SET_POSTIDUSER',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async unlikepost({commit},id){
            try {                              
                await axios.delete(`/api/likes/${id}`)
                commit('SET_LIKE')                  
            } catch (error) {
                console.log(error)
            }
        },
        async postlikepost({commit},data){            
            try {               
                await axios.post(`/api/likes`,{
                    post_id: data.post_id,
                    user_id: data.user_id,
                    status: 1
                })
                commit('SET_LIKE')                  
            } catch (error) {
                console.log(error)
            }
        },
        async getpostiduser({commit},data){            
            try {                
                const res=await axios.get(`/api/postdetaillike/${data.user}/${data.post}`)   
                commit('SET_POSTID',res.data)  
                console.log("data",res.data);                              
            } catch (error) {
                console.log(error)
            }
        },        
        async getpostfind({commit},data){            
            try {                
                const res=await axios.get(`/api/postsearch/${data}`)
                commit('SET_POSTS',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getpostfindid({commit},data){                        
            try {                
                const res=await axios.get(`/api/postsearchlike/${data.user}/${data.content}`)                
                commit('SET_POSTS',res.data)
            } catch (error) {
                console.log(error)
            }
        }
    },
    mutations:{
        SET_POSTS(state,post){
            state.post=post
        },
        SET_POSTID(state,getpostfilter){
            state.getpostfilter = getpostfilter
        },
        SET_POSTIDCLUBS(state,getpostfilterclub){
            state.getpostfilterclub = getpostfilterclub      
        },
        SET_POSTIDUSER(state,getpostuser){
            state.getpostuser = getpostuser      
        },
        SET_LIKE(){

        }
    },
    
}
export default postModules