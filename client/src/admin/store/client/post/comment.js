import axios from 'axios'
const commentModules={
    state:{
        comment:[],
        replycomment:[],
        statuscomment: false   
    },
    getters:{
        comment:state=>state.comment,
        replycomment:state=>state.replycomment,      
    },
    actions:{
        async getcomment({commit},id){
            try {                               
                const res=await axios.get(`/api/comment/getcmtbypost/${id}`)                 
                commit('SET_COMMENTS',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getreplycomment({commit},id){
            try {                               
                const res=await axios.get(`/api/comment/getreply/${id}`)                 
                commit('SET_REPLYCOMMENTS',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async postcommentpostCL({commit},data){            
            try {
                const res = await axios.post(`/api/comments`,{
                   post_id: data.postId,
                   user_id: data.userId,
                   content: data.content
                })
                if (res.status === 200){
                    commit('SET_STATUSCOMMENT')
                }
            } catch (error) {
                console.log(error);
            }
        },
        async postreplycommentpostCL({commit},data){            
            try {
                const res = await axios.post(`http://127.0.0.1:8000/api/replycomments`,{
                   comment_id: data.commentId,
                   user_id: data.userId,
                   content: data.content
                })
                if (res.status === 200){
                    commit('SET_STATUSCOMMENT')
                }
            } catch (error) {
                console.log(error);
            }
        }
    },
    mutations:{
        SET_COMMENTS(state,comment){
            state.comment=comment
        },
        SET_REPLYCOMMENTS(state,replycomment){
            state.replycomment=replycomment
        },
        SET_STATUSCOMMENT(state){
            state.statuscomment = true
        }
    },
    
}
export default commentModules