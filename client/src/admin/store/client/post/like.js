import axios from 'axios'
const likeModules={
    state:{
        likeCL:[],         
    },
    getters:{
        likeCL:state=>state.likeCL,        
    },
    actions:{
        async getlikebyidCL({commit},id){
            try {                               
                const res=await axios.get(`/api/like/getlikebyuser/${id}`)                 
                commit('SET_LIKES',res.data)
            } catch (error) {
                console.log(error)
            }
        }        
    },
    mutations:{
        SET_LIKES(state,likeCL){
            state.likeCL=likeCL
        }        
    },
    
}
export default likeModules