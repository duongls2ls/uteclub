import axios from 'axios'
const userModules={
    state:{
        user:[],
        statusRegister: 0,
        isexituser_CL: false,
        showfee: [],
    },
    getters:{
        users:state=>state.user,
        statusRegister:state=>state.statusRegister,
        checkloginCL:state=>state.isexituser_CL,
        showfee:state=>state.showfee
    },
    actions:{
        async getUser({commit}){
            try {
                const res=await axios.get(`https://605053a8534609001766f777.mockapi.io/club`)
                commit('SET_USERS',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async postUserRegister({commit},data){   
            commit('SET_STATUSOLD')       
            try {
                const res = await axios.post(`/api/users`,{
                  email: data.email,
                  password: data.pass,
                  name: data.name,
                  is_active: 1,
                  role_id: 3
                })
                if (res.status === 200){
                    commit('SET_STATUS');
                }
            } catch (error) {
                console.log(error);
            }
        },
        async login_client({commit},data){
            commit('SET_STATUSRLOGIN')
            try {
                const res = await axios.post(`/api/users/login`,{
                    email: data.emaillogin,
                    password: data.passlogin,                    
                  })
                  if (res.status === 200){
                      commit('SET_STATUSLOGIN');
                      sessionStorage.setItem('tokenlogin',JSON.stringify(res.data))
                  }
            } catch (error) {
                console.log(error);
            }
        },
        async showfeess({commit},idUser){
            try {
                const res=await axios.get(`/api/costactivitydetail/getbyuser/${idUser}`)
                commit('SETLISTFEE',res.data.data)
            } catch (error) {
                console.log(error)
            }
        },
        changestatusout({ commit}){
            commit('SET_STATUSRLOGIN')
        },
        changestatusin({ commit}){
            commit('SET_STATUSLOGIN')
        },
    },
    mutations:{
        SET_USERS(state,user){
            state.user=user
        },
        SET_STATUS(state){
            state.statusRegister = 1
        },
        SET_STATUSOLD(state){
            state.statusRegister = 0
        },
        SET_STATUSLOGIN(state){
            state.isexituser_CL = true
        },
        SET_STATUSRLOGIN(state){
            state.isexituser_CL = false
        },
        SETLISTFEE(state,data){
            state.showfee = data
        }
    },
    
}
export default userModules