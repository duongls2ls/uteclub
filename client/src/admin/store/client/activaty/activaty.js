import axios from 'axios'
const activatyModules={
    state:{
        activity:[],
        detailAct:[]
    },
    getters:{
        activitys:state=>state.activity,
        detailAct:state=>state.detailAct
    },
    actions:{
        async getActivaty({commit}){
            try {
                const res=await axios.get(`/api/activities`)
                commit('SET_ACTIVITYS',res.data.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getDetailAvt({commit},idAvt){
            try {
                const res=await axios.get(`/api/activitie/getactclient/${idAvt}`)
                commit('SET_DETAIL_AVT',res.data.data)
            } catch (error) {
                console.log(error)
            }
        }

    },
    mutations:{
        SET_ACTIVITYS(state,activity){
            state.activity=activity
        },
        SET_DETAIL_AVT(state,detailAct){
            state.detailAct=detailAct
        }

    },
    
}
export default activatyModules