import axios from 'axios'
const listactivatyModules={
    state:{
        listactivaty:[],
        listallactivity: [],
        listActOn_CL:[],
        listActbyClub_CL:[]
    },
    getters:{
        listactivatys:state=>state.listactivaty,
        listallactivity:state=>state.listallactivity,
        listActOn_CL:state=>state.listActOn_CL,
        listActbyClub_CL:state=>state.listActbyClub_CL
    },
    actions:{
        async getListactivaty({commit}){
            try {
                const res=await axios.get(`http://127.0.0.1:8000/api/activities`)
                commit('SET_LISTACTIVATY',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getListAllactivity({commit}){
            try {
                const res=await axios.get(`http://127.0.0.1:8000/api/activitie/getuserbyactall`)
                commit('SET_LISTALLACTIVITY',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getListActOn_CL({commit},idUser){
            try {
                const res=await axios.get(`/api/activitie/getactbyuser/${idUser}`)
                commit('SET_LISTACTON_CL',res.data.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getListActByClub_CL({commit},idUser){
            try {
                const res=await axios.get(`/api/activitie/getactbyclubuser/${idUser}`)
                commit('SET_LISTACTBYCLUB_CL',res.data.data)
            } catch (error) {
                console.log(error)
            }
        },
    },
    mutations:{
        SET_LISTACTIVATY(state,listactivaty){
            state.listactivaty=listactivaty
        },
        SET_LISTALLACTIVITY(state,listallactivity){
            state.listallactivity=listallactivity
        },
        SET_LISTACTON_CL(state,list_act_on){
            state.listActOn_CL=list_act_on
        },
        SET_LISTACTBYCLUB_CL(state,list_act_by_club){
            state.listActbyClub_CL=list_act_by_club
        }
    },
    
}
export default listactivatyModules