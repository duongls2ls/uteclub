import axios from 'axios'
const clubModules={
    state:{
        club:[],
        ischeclregis: 0,
        statusYC: [],
        cluster: [],
        count_user: 0,
        count_activity: 0,
    },
    getters:{
        club:state=>state.club,
        ischeclregis:state=>state.ischeclregis,
        statusYC:state=>state.statusYC,
        clusterCL:state=>state.cluster,
        count_user:state=>state.count_user,
        count_activity:state=>state.count_activity
    },
    actions:{
        async getClub({commit}){
            try {
                const res=await axios.get(`/api/clubs`)
                commit('SET_CLUBS',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async setclubuser({commit},data){                    
            try {
                const res=await axios.post(`/api/detailclubs/`,{
                    club_id: data.club,
                    user_id: data.user,
                    status: 0
                })
                if (res.status === 200){
                    commit('SET_CLUBIDUSER');
                }                
            } catch (error) {
                console.log(error)
            }
        },
        async getClubstatus({commit},data){
            try {
                const res=await axios.get(`/api/statusclub/${data.user}/${data.club}`)
                commit('SET_STATUS',res.data)                
            } catch (error) {
                console.log(error)
            }
        },
        async listclubbyuser({commit},id){
            try {
                const res = await axios.get(`/api/listclubbyuser/${id}`)
                commit('SET_LISTCLUB',res.data)

            } catch (error) {
                console.log(error)
            }
        },
        async countlistuserclub({commit},id){
            try {
                const res = await axios.get(`api/user/getuserbyclub1/${id}`)
                commit('SET_COUNTUSERCLUB',res.data) 

            } catch (error) {
                console.log(error)
            }
        },
        async countlistactivityclub({commit},id){
            try {
                const res = await axios.get(`api/activitie/getactbyclub/${id}`)
                commit('SET_COUNTACTIVITYCLUB',res.data) 

            } catch (error) {
                console.log(error)
            }
        },
    },
    mutations:{
        SET_CLUBS(state,clubs){
            state.clubs=clubs
        },
        SET_CLUBIDUSER(state){
            state.ischeclregis = 1
        },
        SET_STATUS(state,status){
            state.statusYC = status
        },
        SET_LISTCLUB(state,cluster){
            state.cluster = cluster
        },
        SET_COUNTUSERCLUB(state,count_user){
            state.count_user = count_user
        }
        ,
        SET_COUNTACTIVITYCLUB(state,count_activity){
            state.count_activity = count_activity
        }
    },
    
}
export default clubModules