import axios from 'axios'
const listclubModules={
    state:{
        listclub:[],
        clubdata: [],
    },
    getters:{
        listclubs:state=>state.listclub,
        clubdata:state=>state.clubdata,
            
    },
    actions:{
        async getListClubs({commit}){
            try {
                const res=await axios.get(`/api/clubs`)
                commit('SET_LISTCLUBS',res.data)
            } catch (error) {
                console.log(error)
            }
        },
        async getcluboption({commit},id){
            try {
                const res = await axios.get(`/api/clubs/${id}`)            
                commit('GET_CLUBDATA',res.data)
            } catch (error) {
                console.log(error)
            }
        }

    },
    mutations:{
        SET_LISTCLUBS(state,listclub){
            state.listclub=listclub
        },
        GET_CLUBDATA(state,club){
            state.clubdata = club
        }
    },
    
}
export default listclubModules