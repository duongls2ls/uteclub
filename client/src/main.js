import Vue from 'vue'
import router from './router'
import Antd from 'ant-design-vue';
import BootstrapVue from 'bootstrap-vue'
import Vue2Editor from "vue2-editor";
import App from './App'
import 'ant-design-vue/dist/antd.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// import 'bootstrap'
// import 'bootstrap/dist/css/bootstrap.min.css'


import DefaultAdmin from './admin/Layout/Wrappers/baseLayout.vue'
import DefaultClient from './Clients/Layout/Wrappers/layoutbase.vue'
import DefaultClientSite from './Clients/Layout/Wrappers/layoutbasesite'
import Pages from './admin/Layout/Wrappers/pagesLayout.vue'
import Datatable from './admin/utils/Datatable/Datatable.vue'
import store from './admin/store'

window.app = require('./admin/utils/app')

window.axios = require('axios')

axios.defaults.baseURL = process.env.NODE_ENV === 'production'
    ? window.apiUrl
    : app.apiUrl

require('@/admin/store/subscriber')

store.dispatch('login/check')
    .catch(data => console.error(data.message))
    
library.add(fab, fas, far);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;

Vue.use(BootstrapVue)
Vue.use(Vue2Editor);
Vue.use(Antd);
Vue.component('defaultadmin-layout', DefaultAdmin)
Vue.component('defaultclient-layout',DefaultClient),
Vue.component('defaultclientsite-layout',DefaultClientSite)
Vue.component('userpages-layout', Pages)
Vue.component('datatable', Datatable)

router.beforeEach((to, from, next) => {
  let title = process.env.NODE_ENV === 'production'
      ? window.appName
      : app.name

  if (to.meta.title)
    title += ' - ' + to.meta.title

  document.title = title
  next()
})

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App},
});
