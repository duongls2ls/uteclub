<?php

namespace App\Http\Requests\Club;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|string|max:255',
            'slug' => 'bail|required|string',
            'infomation' => 'bail|required|string',
            'user_id' => 'bail|required|integer',
            'banner' => 'bail|required',
        ];
    }
}
