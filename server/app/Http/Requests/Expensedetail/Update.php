<?php

namespace App\Http\Requests\Expensedetail;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'bail|string',
            'expense_id' => 'bail|integer',
            'content' => 'bail|string',
            'amount' => 'bail',
            'status' => 'bail|integer'
        ];
    }
}
