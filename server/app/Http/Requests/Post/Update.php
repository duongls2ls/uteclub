<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'bail|integer',
            'title' => 'bail|string|max:255',
            'content' => 'bail|string',
            'slug' => 'bail|string|max:255',
            'images' => 'bail|string',
            'comment_count' => 'bail|integer',
            'like_count' => 'bail|integer',
            'status' => 'bail|integer',
            'club_id' => 'bail|integer',
        ];
    }
}
