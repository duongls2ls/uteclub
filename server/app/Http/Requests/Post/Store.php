<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'bail|required|integer',
            'title' => 'bail|required|string|max:255',
            'content' => 'bail|required|string',
            'slug' => 'bail|string|max:255',
            'images' => 'bail|string',
            'comment_count' => 'bail|integer',
            'like_count' => 'bail|integer',
            'status' => 'bail|required|integer',
            'club_id' => 'bail|required|integer',
            //
        ];
    }
}
