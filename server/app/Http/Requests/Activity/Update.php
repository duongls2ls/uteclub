<?php

namespace App\Http\Requests\Activity;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|string|max:255',
            'content' => 'bail|required|string',
            'title' => 'bail|required|string',
            'quantity' => 'bail|required|integer',
            'status' => 'bail|required|integer',
            'slug' => 'bail|string|max:255',
            'date_start' => 'required',
            'date_end' => 'required',
            'club_id' => 'bail|required|integer',
            'image' => 'bail|string|',
        ];
    }
}
