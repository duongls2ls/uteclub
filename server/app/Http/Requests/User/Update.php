<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|string|max:255',
            // 'email' => 'bail|required|email|unique:users,email,' . $this->route('user')->id,
            'password' => 'bail|string|min:8|max:255',
            'mobile_number' => 'bail|string|max:255',
            'is_active' => 'bail|boolean',
            'role_id' => 'bail|integer',
            'birthday' => 'bail|string',
            'avatar' => 'bail|string',
            'address' => 'bail|string',
            'gender' => 'bail|boolean',
            'username' => 'bail|string',
            'facebook' => 'bail|string',
            'zalo' => 'bail|string',
            'instagram' => 'bail|string',
        ];
    }
}
