<?php

namespace App\Http\Requests\Like;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_id' => 'bail|required|integer',
            'user_id' => 'bail|required|integer',
            'status' => 'bail|required|boolean'
        ];
    }
}
