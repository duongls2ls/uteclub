<?php

namespace App\Http\Requests\ReplyComment;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment_id' => 'bail|required|integer',
            'user_id' => 'bail|required|integer',
            'content' => 'bail|required|string',
        ];
    }
}
