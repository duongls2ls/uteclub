<?php

namespace App\Http\Controllers\Api;

use App\Club;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Club\Store;
use App\Http\Requests\Club\Update;
use App\Repositories\Interfaces\ClubRepositoryInterface;
use App\User;
use App\Http\Resources\Club as ClubResource;
use Illuminate\Support\Facades\Auth;



class ClubController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(ClubRepositoryInterface $clubRepository)
    {
        return ClubResource::collection($clubRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, ClubRepositoryInterface $clubRepository)
    {
        // return "Dxd";
        // // $file = request()->file('file');
        // // $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
        // // $file->move('image/clubs/', $fileName); 
        // // return $fileName;
        // // die;
       
        // // $request['banner'] = 'public/image/clubs/'.$fileName;
        // return $request;
        // die;
        
        return response()->json([
            'message' => 'Tạo câu lạc bộ thành công',
           'data' => new ClubResource($clubRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Club $club
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Club $club, ClubRepositoryInterface $clubRepository)
    {
        return new ClubResource($clubRepository->details($club));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\Club $club
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Club $club, ClubRepositoryInterface $clubRepository)
    {
        // $file = $request['banner'];
        // $destinationPath = 'public/image/clubs'; // upload path
        // $profileImage = date('YmdHis') . "." . $file->getClientOriginalExtension();
        // $file->move($destinationPath, $profileImage);

        // $request['banner'] = $destinationPath."/".$request['banner']["name"];

        $clubRepository->update($club, $request->validated());

        return response()->json([
            'message' => 'Cập nhật câu lạc bộ thành công',
           'data' => new ClubResource($club)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Club $club
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Club $club, ClubRepositoryInterface $clubRepository)
    {
        $clubRepository->delete($club);

        return response()->json([
           'message' => 'Xóa câu lạc bộ thành công!'
        ]);
    }
    //
    /**
     * Display a listing of the resource.
     *
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getAll(ClubRepositoryInterface $clubRepository)
    {
        return $clubRepository->getAllClub();
    }
}
