<?php

namespace App\Http\Controllers\Api;

use App\Expensedetail;
use App\Http\Controllers\Controller;
use App\Http\Requests\Expensedetail\Store;
use App\Http\Requests\Expensedetail\Update;
use App\Repositories\Interfaces\ExpensedetailRepositoryInterface;
use App\Http\Resources\Expensedetail as ExpensedetailResource;

class ExpensedetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ExpensedetailRepositoryInterface $expensedetailRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(expensedetailRepositoryInterface $expensedetailRepository)
    {
        return ExpensedetailResource::collection($expensedetailRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param ExpensedetailRepositoryInterface $expensedetailRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, ExpensedetailRepositoryInterface $expensedetailRepository)
    {
        return response()->json([
            'message' => 'Tạo chi tiết chi phí thành công',
           'data' => new ExpensedetailResource($expensedetailRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Expensedetail $expensedetail
     * @param ExpensedetailRepositoryInterface $expensedetailRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Expensedetail $expensedetail, ExpensedetailRepositoryInterface $expensedetailRepository)
    {
        return new ExpensedetailResource($expensedetailRepository->details($expensedetail));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\Expensedetail $expensedetail
     * @param ExpensedetailRepositoryInterface $expensedetailRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Expensedetail $expensedetail, ExpensedetailRepositoryInterface $expensedetailRepository)
    {
        $expensedetailRepository->update($expensedetail, $request->validated());

        return response()->json([
            'message' => 'Cập nhật chi tiết chi phí thành công',
           'data' => new ExpensedetailResource($expensedetail)
        ]);
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param \App\Expensedetail $expensedetail
     * @param ExpensedetailRepositoryInterface $expensedetailRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expensedetail $expensedetail, ExpensedetailRepositoryInterface $expensedetailRepository)
    {
        $expensedetailRepository->delete($expensedetail);

        return response()->json([
            'message' => 'Xóa chi tiết chi phí thành công'
        ]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param ExpensedetailRepositoryInterface $expensedetailRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getbydetail(ExpensedetailRepositoryInterface $expensedetaildetailRepository,$id)
    {
        return $expensedetaildetailRepository->getbydetail($id);
    }
}
