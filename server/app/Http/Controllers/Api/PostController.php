<?php

namespace App\Http\Controllers\Api;

use App\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Post\Store;
use App\Http\Requests\Post\Update;
use App\Repositories\Interfaces\PostRepositoryInterface;
use App\User;
use App\Http\Resources\Post as PostResource;
use Illuminate\Support\Facades\Auth;



class PostController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(PostRepositoryInterface $postRepository)
    {
        return $postRepository->datatable();
    }

    /**
     * Display a listing of the resource.
     *
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function postlike(PostRepositoryInterface $postRepository ,$userid)
    {
        return $postRepository->postlike($userid);
    }
    //
    /**
     * Display a listing of the resource.
     *
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function postsearch(PostRepositoryInterface $postRepository,$text)
    {
        return $postRepository->postsearch($text);
    }

    /**
     * Display a listing of the resource.
     *
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function postsearchlike(PostRepositoryInterface $postRepository ,$userid,$text)
    {
        return $postRepository->postsearchlike($userid,$text);
    }
    /**
     * Display the specified resource.
     *
     * @param \App\Post $user
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function postdetaillike(PostRepositoryInterface $postRepository ,$user_id,$post_id)
    {
        return $postRepository->postdetaillike($user_id,$post_id);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param PostRepositoryInterface $postRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, PostRepositoryInterface $postRepository)
    {
        return response()->json([
            'message' => 'Tạo bài đăng thành công',
           'data' => new PostResource($postRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Post $user
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Post $post, PostRepositoryInterface $postRepository)
    {
        return $postRepository->details($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\Post $post
     * @param PostRepositoryInterface $postRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Post $post, PostRepositoryInterface $postRepository)
    {
        $postRepository->update($post, $request->validated());

        return response()->json([
            'message' => 'Cập nhật bài đăng thành công',
           'data' => new PostResource($post)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Post $post
     * @param PostRepositoryInterface $postRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post, PostRepositoryInterface $postRepository)
    {
        $postRepository->delete($post);

        return response()->json([
            'message' => 'Xóa bài đăng thành công'
        ]);
    }
     //
    /**
     * Display a listing of the resource.
     *
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getPostByClub(PostRepositoryInterface $postRepository,$club_id)
    {
        return $postRepository->getpostByClub($club_id);
    }
     /**
     * Display the specified resource.
     *
     * @param \App\Post $user
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function poststatus(PostRepositoryInterface $postRepository,$status,$club_id)
    {
        return $postRepository->poststatus($status,$club_id);
    }
     /**
     * Display a listing of the resource.
     *
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function postclub(PostRepositoryInterface $postRepository ,$club_id)
    {
        return $postRepository->postclub($club_id);
    }
    /**
     * Display a listing of the resource.
     *
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function postuser(PostRepositoryInterface $postRepository ,$user_id)
    {
        return $postRepository->postuser($user_id);
    }
     /**
     * Display a listing of the resource.
     *
     * @param PostRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function postclublike(PostRepositoryInterface $postRepository ,$userid,$club_id)
    {
        return $postRepository->postclublike($userid,$club_id);
    }
}
