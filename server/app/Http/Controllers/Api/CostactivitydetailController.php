<?php

namespace App\Http\Controllers\Api;

use App\Costactivitydetail;
use App\Http\Controllers\Controller;
use App\Http\Requests\Costactivitydetail\Store;
use App\Http\Requests\Costactivitydetail\Update;
use App\Repositories\Interfaces\CostactivitydetailRepositoryInterface;
use App\Http\Resources\Costactivitydetail as CostactivitydetailResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CostactivitydetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CostactivitydetailRepositoryInterface $costactivitydetailRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(CostactivitydetailRepositoryInterface $costactivitydetailRepository)
    {
        return CostactivitydetailResource::collection($costactivitydetailRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param CostactivitydetailRepositoryInterface $costactivitydetailRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, CostactivitydetailRepositoryInterface $costactivitydetailRepository)
    {
        return response()->json([
            'message' => 'Tạo chi tiết chi phí hoạt động thành công',
           'data' => new CostactivitydetailResource($costactivitydetailRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Costactivity $costactivity
     * @param CostactivitydetailRepositoryInterface $costactivitydetailRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Costactivitydetail $costactivitydetail, CostactivitydetailRepositoryInterface $costactivitydetailRepository)
    {
        return new CostactivitydetailResource($costactivitydetailRepository->details($costactivitydetail));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\Costactivitydetail $costactivitydetail
     * @param CostactivitydetailRepositoryInterface $costactivitydetailRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Costactivitydetail $costactivitydetail, CostactivitydetailRepositoryInterface $costactivitydetailRepository)
    {
        $costactivitydetailRepository->update($costactivitydetail, $request->validated());

        return response()->json([
            'message' => 'Cập nhật chi tiết chi phí hoạt động thành công',
           'data' => new CostactivitydetailResource($costactivitydetail)
        ]);
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param \App\Costactivity $costactivity
     * @param CostactivityRepositoryInterface $costactivityRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Costactivitydetail $costactivitydetail, CostactivitydetailRepositoryInterface $costactivitydetailRepository)
    {
        $costactivitydetailRepository->delete($costactivitydetail);

        return response()->json([
            'message' => 'Xóa chi tiết chi phí hoạt động thành công'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @param CostactivitydetailRepositoryInterface $costactivitydetailRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getbydetail(CostactivitydetailRepositoryInterface $costactivitydetailRepository,$id)
    {
        return $costactivitydetailRepository->getbydetail($id);
    }
    /**
     * Display a listing of the resource.
     *
     * @param CostactivitydetailRepositoryInterface $costactivitydetailRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getbyuser(CostactivitydetailRepositoryInterface $costactivitydetailRepository,$id)
    {
        return $costactivitydetailRepository->getbyuser($id);
    }
}
