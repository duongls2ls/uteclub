<?php

namespace App\Http\Controllers\Api;

use App\ReplyComment;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReplyComment\Store;
use App\Http\Requests\ReplyComment\Update;
use App\Repositories\Interfaces\ReplyCommentRepositoryInterface;
use App\User;
use App\Http\Resources\ReplyComment as ReplyCommentResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReplyCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ReplyCommentRepositoryInterface $replycommentRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(ReplyCommentRepositoryInterface $replycommentRepository)
    {
        return ReplyCommentResource::collection($replycommentRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param ReplyCommentRepositoryInterface $commentRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, ReplyCommentRepositoryInterface $replycommentRepository)
    {
        return response()->json([
            'message' => 'Tạo phản hồi bình luận thành công',
           'data' => new ReplyCommentResource($replycommentRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\ReplyComment $comment
     * @param ReplyCommentRepositoryInterface $commentRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(ReplyComment $comment, ReplyCommentRepositoryInterface $replycommentRepository)
    {
        return new ReplyCommentResource($replycommentRepository->details($comment));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\ReplyComment $user
     * @param ReplyCommentRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, ReplyComment $replycomment, ReplyCommentRepositoryInterface $replycommentRepository)
    {
        $replycommentRepository->update($replycomment, $request->validated());

        return response()->json([
            'message' => 'Cập nhật phản hồi bình luận thành công',
           'data' => new ReplyCommentResource($replycomment)
        ]);
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param \App\ReplyComment $replycomment
     * @param CommentRepositoryInterface $commentRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReplyComment $replycomment, ReplyCommentRepositoryInterface $replycommentRepository)
    {
        $replycommentRepository->delete($replycomment);

        return response()->json([
            'message' => 'Xóa phản hồi bình luận thành công'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @param ReplyCommentRepositoryInterface $replycommentRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getReplyByComment(ReplyCommentRepositoryInterface $replycommentRepository,$comment_id)
    {
        return $replycommentRepository->getreplyByComment($comment_id);
    }
}
