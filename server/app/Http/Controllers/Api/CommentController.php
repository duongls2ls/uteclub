<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\Store;
use App\Http\Requests\Comment\Update;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\User;
use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CommentRepositoryInterface $commentRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(CommentRepositoryInterface $commentRepository)
    {
        return CommentResource::collection($commentRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param CommentRepositoryInterface $commentRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, CommentRepositoryInterface $commentRepository)
    {
        return response()->json([
            'message' => 'Tạo bình luận thành công',
           'data' => new CommentResource($commentRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Comment $comment
     * @param CommentRepositoryInterface $commentRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Comment $comment, CommentRepositoryInterface $commentRepository)
    {
        return new CommentResource($commentRepository->details($comment));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\Comment $user
     * @param CommentRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Comment $comment, CommentRepositoryInterface $commentRepository)
    {
        $commentRepository->update($comment, $request->validated());

        return response()->json([
            'message' => 'Cập nhật bình luận thành công',
           'data' => new CommentResource($comment)
        ]);
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param \App\Comment $comment
     * @param CommentRepositoryInterface $commentRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment, CommentRepositoryInterface $commentRepository)
    {
        $commentRepository->delete($comment);

        return response()->json([
            'message' => 'Xóa bình luận thành công'
        ]);
    }
     /**
     * Display a listing of the resource.
     *
     * @param CommentRepositoryInterface $commentRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getCommentByPost(CommentRepositoryInterface $commentRepository,$post_id)
    {
        return $commentRepository->getcommentByPost($post_id);
    }
}
