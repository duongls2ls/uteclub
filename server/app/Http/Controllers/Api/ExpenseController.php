<?php

namespace App\Http\Controllers\Api;

use App\Expense;
use App\Http\Controllers\Controller;
use App\Http\Requests\Expense\Store;
use App\Http\Requests\Expense\Update;
use App\Repositories\Interfaces\ExpenseRepositoryInterface;
use App\Http\Resources\Expense as ExpenseResource;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ExpenseRepositoryInterface $expenseRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(ExpenseRepositoryInterface $expenseRepository)
    {
        return ExpenseResource::collection($expenseRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param ExpenseRepositoryInterface $expenseRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, ExpenseRepositoryInterface $expenseRepository)
    {
        return response()->json([
            'message' => 'Tạo chi phí thành công',
           'data' => new ExpenseResource($expenseRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Expense $expense
     * @param ExpenseRepositoryInterface $expenseRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Expense $expense, ExpenseRepositoryInterface $expenseRepository)
    {
        return new ExpenseResource($expenseRepository->details($expense));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\Expense $expense
     * @param ExpenseRepositoryInterface $expenseRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Expense $expense, ExpenseRepositoryInterface $expenseRepository)
    {
        $expenseRepository->update($expense, $request->validated());

        return response()->json([
            'message' => 'Cập nhật chi phí thành công',
           'data' => new ExpenseResource($expense)
        ]);
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param \App\Expense $expense
     * @param ExpenseRepositoryInterface $expenseRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense, ExpenseRepositoryInterface $expenseRepository)
    {
        $expenseRepository->delete($expense);

        return response()->json([
            'message' => 'Xóa chi phí thành công'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @param ExpenseRepositoryInterface $expenseRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getbyclub(ExpenseRepositoryInterface $expenseRepository,$club_id)
    {
        return $expenseRepository->getbyclub($club_id);
    }
}
