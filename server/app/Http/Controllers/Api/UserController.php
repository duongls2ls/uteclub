<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Store;
use App\Http\Requests\User\Update;
use App\Http\Requests\User\UpdateMe;
use App\Http\Requests\User\Login;
use App\Http\Requests\User\UpdatePassword;
use App\Http\Requests\User\UpdatePin;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(UserRepositoryInterface $userRepository)
    {
        return UserResource::collection($userRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, UserRepositoryInterface $userRepository)
    {
        return response()->json([
           'message' => 'Tạo người dùng thành công',
           'data' => new UserResource($userRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(User $user, UserRepositoryInterface $userRepository)
    {
        return new UserResource($userRepository->details($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\User $user
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, User $user, UserRepositoryInterface $userRepository)
    {
        $userRepository->update($user, $request->validated());

        return response()->json([
            'message' => 'Cập nhật thành công',
           'data' => new UserResource($user)
        ]);
    }

    /**
     * @param UpdateMe $request
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMe(UpdateMe $request, UserRepositoryInterface $userRepository)
    {
        $user = Auth::user();

        $userRepository->update($user, $request->validated());

        return response()->json([
            'message' => 'Cập nhật người dùng thành công',
            'data' => new UserResource($user)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, UserRepositoryInterface $userRepository)
    {
        $userRepository->delete($user);

        return response()->json([
            'message' => 'Xóa người dùng thành công'
        ]);
    }

    /**
     * @param UpdatePassword $request
     * @param User $user
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(UpdatePassword $request, User $user, UserRepositoryInterface $userRepository)
    {
        $userRepository->updatePassword($user, $request->validated());

        return response()->json([
            'message' => 'Cập nhật mật khẩu thành công!'
        ]);
    }

    /**
     * @param UpdatePassword $request
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMyPassword(UpdatePassword $request, UserRepositoryInterface $userRepository)
    {
        $user = Auth::user();

        $userRepository->updatePassword($user, $request->validated());

        return response()->json([
            'message' => 'Cập nhật mật khẩu thành công'
        ]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getUserNotClub(UserRepositoryInterface $userRepository)
    {
        return $userRepository->getuserNotClub();
    }
    /**
     * Display a listing of the resource.
     *
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getUserByClub0(UserRepositoryInterface $userRepository, $club_id)
    {
        // return UserResource::collection($userRepository->getuserByClub0($club_id));
        return $userRepository->getuserByClub0($club_id);
    }

    public function getUserByClub1(UserRepositoryInterface $userRepository, $club_id)
    {
        // return UserResource::collection($userRepository->getuserByClub1($club_id));
        return $userRepository->getuserByClub1($club_id);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param Login $request
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */

    public function Login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            if ($user->role_id == 1 or $user->role_id == 2 or $user->is_active ==0 ) {
                Auth::logout();
                return response()->json([
                    'msg' => 'Tài khoản Mật Khẩu sai',
                ], 401);
            }
            return response()->json([
                'message' => 'Đăng nhập thành công',
                'data' => new UserResource($user)
            ]);
        } else {
            return response()->json([
                'message' => 'Tài khoản Mật Khẩu sai',
            ], 401);
        }
    }
    public function Logout()
    {
        Auth::logout();
        return response()->json([
            'message' => 'Đăng xuất thành công'
        ]);
    }
}
