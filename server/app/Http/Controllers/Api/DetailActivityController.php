<?php

namespace App\Http\Controllers\Api;

use App\DetailActivity;
use App\Http\Controllers\Controller;
use App\Http\Requests\DetailActivity\Store;
use App\Http\Requests\DetailActivity\Update;
use App\Repositories\Interfaces\DetailActivityRepositoryInterface;
use App\User;
use App\Http\Resources\Activity as DetailActivityResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DetailActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param DetailActivityRepositoryInterface $detailactivityRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(DetailActivityRepositoryInterface $detailactivityRepository)
    {
        return DetailActivityResource::collection($detailactivityRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param DetailActivityRepositoryInterface $activityRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, DetailActivityRepositoryInterface $detailactivityRepository)
    {
        return response()->json([
            'message' => 'Tạo chi tiêt hoạt động thành công',
           'data' => new DetailActivityResource($detailactivityRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\DetailActivity $detailactivity
     * @param DetailActivityRepositoryInterface $detailactivityRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(DetailActivity $detailactivity, DetailActivityRepositoryInterface $detailactivityRepository)
    {
        return new DetailActivityResource($detailactivityRepository->details($detailactivity));
    }
     /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\DetailActivity $user
     * @param DetailRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, DetailActivity $detailactivity, DetailActivityRepositoryInterface $detailactivityRepository)
    {
        $detailactivityRepository->update($detailactivity, $request->validated());

        return response()->json([
            'message' => 'Cập nhật chi tiêt hoạt động thành công',
           'data' => new DetailActivityResource($detailactivity)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\DetailActivity $user
     * @param DetailActivityRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetailActivity $detailactivity, DetailActivityRepositoryInterface $detailactivityRepository)
    {
        $detailactivityRepository->delete($detailactivity);

        return response()->json([
            'message' => 'Xóa chi tiêt hoạt động thành công'
        ]);
    }
}
