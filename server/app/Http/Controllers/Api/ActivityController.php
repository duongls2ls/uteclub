<?php

namespace App\Http\Controllers\Api;

use App\Activity;
use App\Http\Controllers\Controller;
use App\Http\Requests\Activity\Store;
use App\Http\Requests\Activity\Update;
use App\Repositories\Interfaces\ActivityRepositoryInterface;
use App\User;
use App\Http\Resources\Activity as ActivityResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ActivityRepositoryInterface $activityRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(ActivityRepositoryInterface $activityRepository)
    {
        return ActivityResource::collection($activityRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param ActivityRepositoryInterface $activityRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, ActivityRepositoryInterface $activityRepository)
    {
        return response()->json([
           'message' => 'Tạo hoạt động thành công!',
           'data' => new ActivityResource($activityRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Activity $activity
     * @param ActivityRepositoryInterface $activityRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Activity $activity, ActivityRepositoryInterface $activityRepository)
    {
        return new ActivityResource($activityRepository->details($activity));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\Activity $user
     * @param UserRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Activity $activity, ActivityRepositoryInterface $activityRepository)
    {
        $activityRepository->update($activity, $request->validated());

        return response()->json([
            'message' => 'Cập nhật hoạt động thành công',
           'data' => new ActivityResource($activity)
        ]);
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param \App\Activity $comment
     * @param ActivityRepositoryInterface $commentRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity, ActivityRepositoryInterface $activityRepository)
    {
        $activityRepository->delete($activity);

        return response()->json([
            'message' => 'Xóa hoạt động thành công'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param ActivityRepositoryInterface $activityRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getActByClub(ActivityRepositoryInterface $activityRepository,$club_id)
    {
        return $activityRepository->getactByClub($club_id);
    }

    public function getUserByAct(ActivityRepositoryInterface $activityRepository,$activite_id)
    {
        return $activityRepository->getuserByAct($activite_id);
    }

    public function getActByClubData(ActivityRepositoryInterface $activityRepository,$club_id)
    {
        return ActivityResource::collection($activityRepository->getactByClubData($club_id));
    }

    public function getUserByActData(ActivityRepositoryInterface $activityRepository,$activite_id)
    {
        return $activityRepository->getuserByActData($activite_id);
    }
    public function getUserByActAll(ActivityRepositoryInterface $activityRepository)
    {
        return $activityRepository->getactByClubAll();
    }
    public function getActClient(ActivityRepositoryInterface $activityRepository, $isAct)
    {
        return $activityRepository->getActClient($isAct);
    }
    public function getActbyUser(ActivityRepositoryInterface $activityRepository, $userid)
    {
        // return ActivityResource::collection( $activityRepository->getActbyUser($userid));
        return $activityRepository->getActbyUser($userid);
    }

    public function getActbyClubUser(ActivityRepositoryInterface $activityRepository, $userid)
    {
        // return ActivityResource::collection( $activityRepository->getActbyUser($userid));
        return $activityRepository->getActbyClubUser($userid);
    }
    public function reportact(ActivityRepositoryInterface $activityRepository)
    {
        return $activityRepository->reportact();
    }
}
