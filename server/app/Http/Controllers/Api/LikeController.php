<?php

namespace App\Http\Controllers\Api;

use App\Like;
use App\Http\Controllers\Controller;
use App\Http\Requests\Like\Store;
use App\Http\Requests\Like\Update;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use App\User;
use App\Http\Resources\Like as likeResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param LikeRepositoryInterface $likeRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(LikeRepositoryInterface $likeRepository)
    {
        return LikeResource::collection($likeRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param LikeRepositoryInterface $likeRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, LikeRepositoryInterface $likeRepository)
    {
        return response()->json([
            'message' => 'Tạo lượt thích thành công',
           'data' => new LikeResource($likeRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Like $like
     * @param LikeRepositoryInterface $likeRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Like $like, LikeRepositoryInterface $likeRepository)
    {
        return new LikeResource($likeRepository->details($like));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\Like $like
     * @param LikeRepositoryInterface $likeRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Like $like, LikeRepositoryInterface $likeRepository)
    {
        $likeRepository->update($like, $request->validated());

        return response()->json([
            'message' => 'Cập nhật lượt thích thành công',
           'data' => new LikeResource($like)
        ]);
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param \App\Like $like
     * @param LikeRepositoryInterface $likeRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Like $like, LikeRepositoryInterface $likeRepository)
    {
        $likeRepository->delete($like);

        return response()->json([
            'message' => 'Xóa lượt thích thành công'
        ]);
    }
     /**
     * Display a listing of the resource.
     *
     * @param LikeRepositoryInterface $likeRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getLikeByUser(LikeRepositoryInterface $likeRepository,$user_id)
    {
        return LikeResource::collection($likeRepository->getLikeByUser($user_id));
    // return $LikeRepository->getLikeByUser($user_id);
    }
}
