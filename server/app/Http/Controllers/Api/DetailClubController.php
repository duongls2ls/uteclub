<?php

namespace App\Http\Controllers\Api;

use App\DetailClub;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\DetailClub\Store;
use App\Http\Requests\DetailClub\Update;
use App\Repositories\Interfaces\DetailClubRepositoryInterface;
use App\User;
use App\Http\Resources\DetailClub as DetailClubResource;
use Illuminate\Support\Facades\Auth;



class DetailClubController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @param DetailClubRepositoryInterface $detailclubRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(DetailClubRepositoryInterface $detailclubRepository)
    {
        return DetailClubResource::collection($detailclubRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, DetailClubRepositoryInterface $detailclubRepository)
    {
        return response()->json([
            'message' => 'Tạo chi tiết câu lạc bộ thành công',
           'data' => new DetailClubResource($detailclubRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\DetailClub $detailclub
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(DetailClub $detailclub, DetailClubRepositoryInterface $detailclubRepository)
    {
        return new DetailClubResource($detailclubRepository->details($detailclub));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\DetailClub $DetailClub
     * @param ClubRepositoryInterface $clubRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, DetailClub $detailclub, DetailClubRepositoryInterface $detailclubRepository)
    {
        $detailclubRepository->update($detailclub, $request->validated());

        return response()->json([
            'message' => 'Cập nhật chi tiết câu lạc bộ thành công',
           'data' => new DetailClubResource($detailclub)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\DetailClub $user
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetailClub $detailclub, DetailClubRepositoryInterface $detailclubRepository)
    {
        $detailclubRepository->delete($detailclub);

        return response()->json([
            'message' => 'Xóa chi tiết câu lạc bộ thành công'
        ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param \App\DetailClub $user
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function StatusClub(DetailClubRepositoryInterface $detailclubRepository ,$user_id,$club_id)
    {
        return $detailclubRepository->statusclub($user_id,$club_id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param \App\DetailClub $user
     * @param ClubRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function listbyuser(DetailClubRepositoryInterface $detailclubRepository ,$user_id)
    {
        return $detailclubRepository->listbyuser($user_id);
    }
}
