<?php

namespace App\Http\Controllers\Api;

use App\PageConnect;
use App\Http\Controllers\Controller;
use App\Http\Requests\PageConnect\Store;
use App\Http\Requests\PageConnect\Update;
use App\Repositories\Interfaces\PageConnectRepositoryInterface;
use App\User;
use App\Http\Resources\PageConnect as PageConnectResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageConnectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PageConnectRepositoryInterface $pageconnectRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(PageConnectRepositoryInterface $pageconnectRepository)
    {
        return PageConnectResource::collection($pageconnectRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param PageConnectRepositoryInterface $pageconnectRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, PageConnectRepositoryInterface $pageconnectRepository)
    {
        return response()->json([
            'message' => 'Tạo trang kết nối thành công',
           'data' => new PageConnectResource($pageconnectRepository->create($request->validated()))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\PageConnect $pageconnect
     * @param PageConnectRepositoryInterface $pageconnectRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(PageConnect $pageconnect, PageConnectRepositoryInterface $pageconnectRepository)
    {
        return new PageConnectResource($pageconnectRepository->details($pageconnect));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\PageConnect $pageconnect
     * @param PageConnectRepositoryInterface $pageconnectRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, PageConnect $pageconnect, PageConnectRepositoryInterface $pageconnectRepository)
    {
        $pageconnectRepository->update($pageconnect, $request->validated());

        return response()->json([
            'message' => 'Cập nhật trang kết nối thành công',
           'data' => new PageConnectResource($pageconnect)
        ]);
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param \App\PageConnect $pageconnect
     * @param PageConnectRepositoryInterface $pageconnectRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageConnect $pageconnect, PageConnectRepositoryInterface $pageconnectRepository)
    {
        $pageconnectRepository->delete($pageconnect);

        return response()->json([
            'message' => 'Xóa trang kết nối thành công'
        ]);
    }
}
