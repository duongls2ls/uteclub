<?php

namespace App\Http\Controllers\Api;

use App\Costactivity;
use App\Http\Controllers\Controller;
use App\Http\Requests\Costactivity\Store;
use App\Http\Requests\Costactivity\Update;
use App\Repositories\Interfaces\CostactivityRepositoryInterface;
use App\Http\Resources\Costactivity as CostactivityResource;
use App\DetailClub;
use App\Costactivitydetail;
use App\Datatable\Datatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CostactivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CostactivityRepositoryInterface $costactivityRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function index(CostactivityRepositoryInterface $costactivityRepository)
    {
        return CostactivityResource::collection($costactivityRepository->datatable());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     * @param CostactivityRepositoryInterface $costactivityRepository
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request, CostactivityRepositoryInterface $costactivityRepository)
    {
        $mid = $costactivityRepository->create($request->validated());

        $query_club = DetailClub::select()->where('club_id', '=', $request['club_id']);
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $data_club->filterBy([
        ]);
        $clubs = $data_club->get()->toArray();
        foreach ($clubs['data'] as $key => $club) {
            $insert = [
                'costactivity_id' => $mid['id'],
                'user_id' => $club['user_id'],
                'amount'=>$request['quota'],
                'status' => 0
            ];
            
            Costactivitydetail::create($insert);
        }
        return response()->json([
            'message' => 'Tạo chi phí hoạt động thành công',
           'data' => new CostactivityResource($mid)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Costactivity $costactivity
     * @param CostactivityRepositoryInterface $costactivityRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function show(Costactivity $costactivity, CostactivityRepositoryInterface $costactivityRepository)
    {
        return new CostactivityResource($costactivityRepository->details($costactivity));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param \App\Costactivity $user
     * @param CostactivityRepositoryInterface $userRepository
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Costactivity $costactivity, CostactivityRepositoryInterface $costactivityRepository)
    {
        $costactivityRepository->update($costactivity, $request->validated());

        return response()->json([
            'message' => 'Cập nhật chi phí hoạt động thành công',
           'data' => new CostactivityResource($costactivity)
        ]);
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param \App\Costactivity $costactivity
     * @param CostactivityRepositoryInterface $costactivityRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Costactivity $costactivity, CostactivityRepositoryInterface $costactivityRepository)
    {
        $costactivityRepository->delete($costactivity);

        return response()->json([
            'message' => 'Cập nhật chi phí hoạt động thành công',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @param CostactivityRepositoryInterface $costactivityRepository
     * @return \Illuminate\Http\Resources\Json\JsonResource
     */
    public function getbyclub(CostactivityRepositoryInterface $costactivityRepository,$club_id)
    {
        return $costactivityRepository->getbyclub($club_id);
    }
}
