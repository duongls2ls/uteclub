<?php

namespace App\Repositories;

use App\DetailActivity;
use App\Datatable\Datatable;
use App\Http\Resources\DetailActivity as ResourcesDetailActivity;
use App\Repositories\Interfaces\DetailActivityRepositoryInterface;

class DetailActivityRepository implements DetailActivityRepositoryInterface
{
    public function datatable()
    {
        $query = DetailActivity::select();
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
            // 'user_id',
            // 'title',
            // 'content',
            // 'slug',
            // 'comment_count',
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return DetailActivity::create($data);
    }

    public function details(DetailActivity $detailActivity)
    {
       return $detailActivity;
    }
    public function update(DetailActivity $detailActivity, $data)
    {
        return $detailActivity->update($data);
    }
    public function delete(DetailActivity $detailActivity)
    {
        return $detailActivity->delete();
    }
}