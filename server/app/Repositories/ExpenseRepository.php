<?php

namespace App\Repositories;

use App\Expense;
use App\Club;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\ExpenseRepositoryInterface;

class ExpenseRepository implements ExpenseRepositoryInterface
{
    public function datatable()
    {
        $query = Expense::select();
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return Expense::create($data);
    }

    public function details(Expense $expense)
    {
       return $expense;
    }

    public function update(Expense $expense, $data)
    {
        return $expense->update($data);
    }

    public function delete(Expense $expense)
    {
        return $expense->delete();
    }
    public function getbyclub($idClub)
    {
        $data['data'] = [];
        $query_expense = Expense::select()->where('club_id', '=', $idClub);

        $data_expense = new Datatable($query_expense);

        $data_expense->latest();
        $data_expense->filterBy([
            'title',
            'content',
            'amount'
        ]);
        $expenses = $data_expense->get()->toArray();
        $links = array(
            'first' => $expenses['first_page_url'],
            'last' => $expenses['last_page_url'],
            'next' => $expenses['next_page_url'],
            'prev' =>$expenses['prev_page_url']
        );
        $meta = array(
            'current_page' => $expenses['current_page'],
            'from' => $expenses['from'],
            'last_page' => $expenses['last_page'],
            'path' =>$expenses['path'],
            'per_page' => $expenses['per_page'],
            'to' =>$expenses['to'],
            'total' =>$expenses['total']
        );
        $query_club = Club::select([
            'id',
            'name'
        ])->where('id', '=', $idClub);

        $data_club = new Datatable($query_club);

        $data_club->latest();
        $data_club->filterBy([
            'id',
            'name',
        ]);
        $club = $data_club->get()->toArray();
        $middle = [];
        if(count($expenses['data'])>0){
            foreach ($expenses['data'] as $expense) {
                $expense['club'] = $club['data'];
                $middle[] = $expense;
            }
        }
        $data['data'] = $middle;
        $data['links'] = $links;
        $data['meta'] = $meta;
        
        return json_encode($data);
    }
}