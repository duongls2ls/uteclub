<?php

namespace App\Repositories;

use App\PageConnect;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\PageConnectRepositoryInterface;

class PageConnectRepository implements PageConnectRepositoryInterface
{
    public function datatable()
    {
        $query = PageConnect::select();
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
            'name'
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return PageConnect::create($data);
    }

    public function details(PageConnect $pageConnect)
    {
       return $pageConnect;
    }

    public function update(PageConnect $pageConnect, $data)
    {
        return $pageConnect->update($data);
    }

    public function delete(PageConnect $pageConnect)
    {
        return $pageConnect->delete();
    }
}