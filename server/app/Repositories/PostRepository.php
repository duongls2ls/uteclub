<?php

namespace App\Repositories;

use App\Post;
use App\User;
use App\Club;
use App\Like;
use App\Comment;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\PostRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class PostRepository implements PostRepositoryInterface
{
    public function datatable()
    {
        $query_post = Post::select()->where('status','=',1);
        $data_post = new Datatable($query_post);
        $data_post->latest();
        $array_posts = $data_post->get()->toArray();

        $query_user = User::select();
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $array_users = $data_user->get()->toArray();

        $query_club = Club::select();
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $array_clubs = $data_club->get()->toArray();
       
        //
        $data = array();
        $middle = array();
        foreach ($array_posts['data'] as $post) {
            foreach ($array_users['data'] as $user) {
                if($post['user_id']==$user['id']){
                    $post['user']= $user;
                    break;
                }
            }
            foreach ($array_clubs['data'] as $club) {
                if($post['club_id']==$club['id']){
                    $post['club']= $club;
                    break;
                }
            }
            //like
            $query_like= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1);
                
            $data_like = new Datatable($query_like);

            $data_like->latest();
            $data_like->filterBy([
            ]);
            $array_like = $data_like->get()->toArray();
            $post['like'] = count($array_like['data']);
            //comment
            $query_cmt = Comment::select()->where('post_id', '=', $post['id']);;
        
            $data_cmt = new Datatable($query_cmt);

            $data_cmt->latest();
            $data_cmt->filterBy([
            ]);
            $array_cmt = $data_cmt->get()->toArray();
            $post['comment'] = count($array_cmt['data']);
            //
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $time1 =  date('Y-m-d H:i:s');
            $time2 =  date("Y-m-d H:i:s", strtotime($post['created_at']));
            $diff = abs(strtotime($time1) - strtotime($time2));
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
            $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
            $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
            $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

            if($diff < 60 and $diff > 0){
                $time = $seconds." giây";
            }
            if($diff < 3600 and $diff >= 60){
                $time = $minutes." phút";
            }
            if($diff < 3600*24 and $diff >= 3600){
                $time = $hours." giờ";
            }
            if($diff >= 3600*24 and $diff < 3600*24*30){
                $time = $days." ngày";
            }
            if($diff < 3600*24*365 and $diff >= 3600*24*30){
                $time = $months." tháng";
            }
            if($diff >= 3600*24*365){
                $time = $years." năm";
            }
            $post['time']=$time;
            $arraytmp = array();
            if (json_decode($post['images']) != null || json_decode($post['images']) != ''){
                foreach (json_decode($post['images']) as $key => $value) {
                    array_push($arraytmp, $value);
                }
            }
            $post['images_arr'] = $arraytmp;
            $middle[] = $post;
            $data['data'] =  $middle;
        }
     
        return json_encode($data);
        
    }

    public function create($data)
    {
        return Post::create($data);
    }

    public function details(Post $post)
    {
        $array_post = $post->toArray();
    
        //like
        $query_like= Like::select()->where('post_id', '=', $array_post['id'])->where('status', '=', 1);
        
        $data_like = new Datatable($query_like);

        $data_like->latest();
        $data_like->filterBy([
        ]);
        $array_like = $data_like->get()->toArray();
        $array_post['like'] = count($array_like['data']);
        //
        $arraytmp = array();
        if (json_decode($array_post['images']) != null || json_decode($array_post['images']) != ''){
            foreach (json_decode($array_post['images']) as $key => $value) {
                array_push($arraytmp, $value);
            }
        }
        $array_post['images_arr'] = $arraytmp;
         $query_cmt = Comment::select()->where('post_id', '=', $array_post['id']);;
        
         $data_cmt = new Datatable($query_cmt);

         $data_cmt->latest();
         $data_cmt->filterBy([
         ]);
         $array_cmt = $data_cmt->get()->toArray();
         $array_post['comment'] = count($array_cmt['data']);
        //
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $time1 =  date('Y-m-d H:i:s');
        $time2 =  date("Y-m-d H:i:s", strtotime($array_post['created_at']));
        $diff = abs(strtotime($time1) - strtotime($time2));
        
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
        $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
        $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
        $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

        if($diff < 60 and $diff > 0){
            $time = $seconds." giây";
        }
        if($diff < 3600 and $diff >= 60){
            $time = $minutes." phút";
        }
        if($diff < 3600*24 and $diff >= 3600){
            $time = $hours." giờ";
        }
        if($diff >= 3600*24 and $diff < 3600*24*30){
            $time = $days." ngày";
        }
        if($diff < 3600*24*365 and $diff >= 3600*24*30){
            $time = $months." tháng";
        }
        if($diff >= 3600*24*365){
            $time = $years." năm";
        }
        $array_post['time']=$time;

        $query_user = User::select();
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $array_users = $data_user->get()->toArray();

        $query_club = Club::select();
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $array_clubs = $data_club->get()->toArray();


        $data = array();
        foreach ($array_users['data'] as $user) {
            if($array_post['user_id']==$user['id']){
                $array_post['user']= $user;
                break;
            }
        }
        foreach ($array_clubs['data'] as $club) {
            if($array_post['club_id']==$club['id']){
                $array_post['club']= $club;
                break;
            }
        }
        $data['data'] =  $array_post;

        return json_encode($data);
    }

    public function update(Post $post, $data)
    {
        return $post->update($data);
    }

    public function delete(Post $post)
    {
        return $post->delete();
    }
    public function getpostByClub($idClub)
    {
        $query_post = Post::select()->where('club_id', '=', $idClub)->where('status','=',1);
        $data_post = new Datatable($query_post);
        $data_post->latest();
        $array_posts = $data_post->get()->toArray();

        $query_user = User::select();
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $array_users = $data_user->get()->toArray();

        $query_club = Club::select()->where('id', '=', $idClub);
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $array_clubs = $data_club->get()->toArray();
       
        $data = array();
        $middle = array();
        foreach ($array_posts['data'] as $post) {
            foreach ($array_users['data'] as $user) {
                if($post['user_id']==$user['id']){
                    $post['user']= $user;
                    $post['club']=  $array_clubs['data'];
                    break;
                }
            }
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $time1 =  date('Y-m-d H:i:s');
            $time2 =  date("Y-m-d H:i:s", strtotime($post['created_at']));
            $diff = abs(strtotime($time1) - strtotime($time2));
            $post['time']=$diff;

            $arraytmp = array();
            if (json_decode($post['images']) != null || json_decode($post['images']) != ''){
                foreach (json_decode($post['images']) as $key => $value) {
                    array_push($arraytmp, $value);
                }
            }
            $post['images_arr'] = $arraytmp;
            $middle[] = $post;
            $data['data'] =  $middle;
        }
        return json_encode($data);
    }
    public function postlike($idUser)
    {
        $query_post = Post::select()->where('status','=',1);
        $data_post = new Datatable($query_post);
        $data_post->latest();
        $array_posts = $data_post->get()->toArray();

        $query_user = User::select();
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $array_users = $data_user->get()->toArray();

        $query_club = Club::select();
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $array_clubs = $data_club->get()->toArray();
       
        //
        $data = array();
        $middle = array();
        foreach ($array_posts['data'] as $post) {
            foreach ($array_users['data'] as $user) {
                if($post['user_id']==$user['id']){
                    $post['user']= $user;
                    break;
                }
            }
            foreach ($array_clubs['data'] as $club) {
                if($post['club_id']==$club['id']){
                    $post['club']= $club;
                    break;
                }
            }
            //like
            $query_like= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1);
                
            $data_like = new Datatable($query_like);

            $data_like->latest();
            $data_like->filterBy([
            ]);
            $array_like = $data_like->get()->toArray();
            $post['like'] = count($array_like['data']);
            //like status
            //like
            $query_likeS= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1)->where('user_id', '=', $idUser);
                
            $data_likeS = new Datatable($query_likeS);

            $data_likeS->latest();
            $data_likeS->filterBy([
            ]);
            $array_likeS = $data_likeS->get()->toArray();
            if(count($array_likeS['data'])>0){
                $post['likeStatus'] = $array_likeS['data']['0']['status'];
                $post['idLike'] = $array_likeS['data']['0']['id'];
            }else{
                $post['likeStatus'] = null;
                $post['idLike'] = null;
            }
            //comment
            $query_cmt = Comment::select()->where('post_id', '=', $post['id']);;
        
            $data_cmt = new Datatable($query_cmt);

            $data_cmt->latest();
            $data_cmt->filterBy([
            ]);
            $array_cmt = $data_cmt->get()->toArray();
            $post['comment'] = count($array_cmt['data']);
            //
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $time1 =  date('Y-m-d H:i:s');
            $time2 =  date("Y-m-d H:i:s", strtotime($post['created_at']));
            $diff = abs(strtotime($time1) - strtotime($time2));
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
            $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
            $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
            $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

            if($diff < 60 and $diff > 0){
                $time = $seconds." giây";
            }
            if($diff < 3600 and $diff >= 60){
                $time = $minutes." phút";
            }
            if($diff < 3600*24 and $diff >= 3600){
                $time = $hours." giờ";
            }
            if($diff >= 3600*24 and $diff < 3600*24*30){
                $time = $days." ngày";
            }
            if($diff < 3600*24*365 and $diff >= 3600*24*30){
                $time = $months." tháng";
            }
            if($diff >= 3600*24*365){
                $time = $years." năm";
            }
            $post['time']=$time;
            $arraytmp = array();
            if (json_decode($post['images']) != null || json_decode($post['images']) != ''){
                foreach (json_decode($post['images']) as $key => $value) {
                    array_push($arraytmp, $value);
                }
            }
            $post['images_arr'] = $arraytmp;
            $middle[] = $post;
            $data['data'] =  $middle;
        }
     
        return json_encode($data);
        
    }
    public function postdetaillike($idUser,$idPost)
    {
        $data['data'] = [];
        $query_post= Post::select()->where('id', '=', $idPost)->where('status', '=', 1);
        
        $data_post = new Datatable($query_post);

        $data_post->latest();
        $data_post->filterBy([
        ]);
        $array_post = $data_post->get()->toArray();
        if(count($array_post['data'])>0){
            
            //like
            $query_like= Like::select()->where('post_id', '=', $array_post['data'][0]['id'])->where('status', '=', 1);
            
            $data_like = new Datatable($query_like);

            $data_like->latest();
            $data_like->filterBy([
            ]);
            $array_like = $data_like->get()->toArray();
            $array_post['data'][0]['like'] = count($array_like['data']);
            //likestatus
            $query_likeS= Like::select()->where('post_id', '=', $idPost)->where('status', '=', 1)->where('user_id', '=', $idUser);
                    
            $data_likeS = new Datatable($query_likeS);

            $data_likeS->latest();
            $data_likeS->filterBy([
            ]);
            $array_likeS = $data_likeS->get()->toArray();
            if(count($array_likeS['data'])>0){
                $array_post['data'][0]['likeStatus'] = $array_likeS['data'][0]['status'];
                $array_post['data'][0]['idLike'] = $array_likeS['data'][0]['id'];
            }else{
                $array_post['data'][0]['likeStatus'] = null;
                $array_post['data'][0]['idLike'] = null;
            }
            //comment
            //comment
            $query_cmt = Comment::select()->where('post_id', '=', $array_post['data'][0]['id']);;
            
            $data_cmt = new Datatable($query_cmt);

            $data_cmt->latest();
            $data_cmt->filterBy([
            ]);
            $array_cmt = $data_cmt->get()->toArray();
            $array_post['data'][0]['comment'] = count($array_cmt['data']);
            //
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $time1 =  date('Y-m-d H:i:s');
            $time2 =  date("Y-m-d H:i:s", strtotime($array_post['data'][0]['created_at']));
            $diff = abs(strtotime($time1) - strtotime($time2));
            
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
            $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
            $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
            $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

            if($diff < 60 and $diff > 0){
                $time = $seconds." giây";
            }
            if($diff < 3600 and $diff >= 60){
                $time = $minutes." phút";
            }
            if($diff < 3600*24 and $diff >= 3600){
                $time = $hours." giờ";
            }
            if($diff >= 3600*24 and $diff < 3600*24*30){
                $time = $days." ngày";
            }
            if($diff < 3600*24*365 and $diff >= 3600*24*30){
                $time = $months." tháng";
            }
            if($diff >= 3600*24*365){
                $time = $years." năm";
            }
            $array_post['data'][0]['time']=$time;

            $query_user = User::select();
            $data_user = new Datatable($query_user);
            $data_user->latest();
            $array_users = $data_user->get()->toArray();

            $query_club = Club::select();
            $data_club = new Datatable($query_club);
            $data_club->latest();
            $array_clubs = $data_club->get()->toArray();


            $data = array();
            foreach ($array_users['data'] as $user) {
                if($array_post['data'][0]['user_id']==$user['id']){
                    $array_post['data'][0]['user']= $user;
                    break;
                }
            }
            foreach ($array_clubs['data'] as $club) {
                if($array_post['data'][0]['club_id']==$club['id']){
                    $array_post['data'][0]['club']= $club;
                    break;
                }
            }
            $arraytmp = array();
            if (json_decode($array_post['data'][0]['images']) != null || json_decode($array_post['data'][0]['images']) != ''){
                foreach (json_decode($array_post['data'][0]['images']) as $key => $value) {
                    array_push($arraytmp, $value);
                }
            }
            $array_post['data'][0]['images_arr'] = $arraytmp;
            $data['data'] =  $array_post['data'][0];
        }
        

        return json_encode($data);
    }
    public function postsearchlike($idUser,$text)
    {
        $data['data'] = [];
        $text = '%'.$text.'%';
        $query_post = Post::select()->where('title', 'like', $text)->where('content', 'like', $text)->where('status','=',1);
        $data_post = new Datatable($query_post);
        $data_post->latest();
        $array_posts = $data_post->get()->toArray();

        $query_user = User::select();
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $array_users = $data_user->get()->toArray();

        $query_club = Club::select();
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $array_clubs = $data_club->get()->toArray();
       
        //
        if(count($array_posts['data'])>0){
            $data = array();
            $middle = array();
            foreach ($array_posts['data'] as $post) {
                foreach ($array_users['data'] as $user) {
                    if($post['user_id']==$user['id']){
                        $post['user']= $user;
                        break;
                    }
                }
                foreach ($array_clubs['data'] as $club) {
                    if($post['club_id']==$club['id']){
                        $post['club']= $club;
                        break;
                    }
                }
                //like
                $query_like= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1);
                    
                $data_like = new Datatable($query_like);

                $data_like->latest();
                $data_like->filterBy([
                ]);
                $array_like = $data_like->get()->toArray();
                $post['like'] = count($array_like['data']);
                //like status
                //like
                $query_likeS= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1)->where('user_id', '=', $idUser);
                    
                $data_likeS = new Datatable($query_likeS);

                $data_likeS->latest();
                $data_likeS->filterBy([
                ]);
                $array_likeS = $data_likeS->get()->toArray();
                if(count($array_likeS['data'])>0){
                    $post['likeStatus'] = $array_likeS['data']['0']['status'];
                    $post['idLike'] = $array_likeS['data']['0']['id'];
                }else{
                    $post['likeStatus'] = null;
                    $post['idLike'] = null;
                }
                //comment
                $query_cmt = Comment::select()->where('post_id', '=', $post['id']);;
            
                $data_cmt = new Datatable($query_cmt);

                $data_cmt->latest();
                $data_cmt->filterBy([
                ]);
                $array_cmt = $data_cmt->get()->toArray();
                $post['comment'] = count($array_cmt['data']);
                //
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $time1 =  date('Y-m-d H:i:s');
                $time2 =  date("Y-m-d H:i:s", strtotime($post['created_at']));
                $diff = abs(strtotime($time1) - strtotime($time2));
                $years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
                $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
                $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
                $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

                if($diff < 60 and $diff > 0){
                    $time = $seconds." giây";
                }
                if($diff < 3600 and $diff >= 60){
                    $time = $minutes." phút";
                }
                if($diff < 3600*24 and $diff >= 3600){
                    $time = $hours." giờ";
                }
                if($diff >= 3600*24 and $diff < 3600*24*30){
                    $time = $days." ngày";
                }
                if($diff < 3600*24*365 and $diff >= 3600*24*30){
                    $time = $months." tháng";
                }
                if($diff >= 3600*24*365){
                    $time = $years." năm";
                }
                $post['time']=$time;
                $arraytmp = array();
                if (json_decode($post['images']) != null || json_decode($post['images']) != ''){
                    foreach (json_decode($post['images']) as $key => $value) {
                        array_push($arraytmp, $value);
                    }
                }
                $post['images_arr'] = $arraytmp;
                $middle[] = $post;
                $data['data'] =  $middle;
            }
        }
     
        return json_encode($data);
        
    }
    public function postsearch($text)
    {
        $data['data'] = [];
        $text = '%'.$text.'%';
        $query_post = Post::select()->where('title', 'like', $text)->where('content', 'like', $text)->where('status','=',1);
        $data_post = new Datatable($query_post);
        $data_post->latest();
        $array_posts = $data_post->get()->toArray();

        $query_user = User::select();
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $array_users = $data_user->get()->toArray();

        $query_club = Club::select();
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $array_clubs = $data_club->get()->toArray();
       
        //
        if(count($array_posts['data'])>0){
            $data = array();
            $middle = array();
            foreach ($array_posts['data'] as $post) {
                foreach ($array_users['data'] as $user) {
                    if($post['user_id']==$user['id']){
                        $post['user']= $user;
                        break;
                    }
                }
                foreach ($array_clubs['data'] as $club) {
                    if($post['club_id']==$club['id']){
                        $post['club']= $club;
                        break;
                    }
                }
                //like
                $query_like= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1);
                    
                $data_like = new Datatable($query_like);

                $data_like->latest();
                $data_like->filterBy([
                ]);
                $array_like = $data_like->get()->toArray();
                $post['like'] = count($array_like['data']);
                //comment
                $query_cmt = Comment::select()->where('post_id', '=', $post['id']);;
            
                $data_cmt = new Datatable($query_cmt);

                $data_cmt->latest();
                $data_cmt->filterBy([
                ]);
                $array_cmt = $data_cmt->get()->toArray();
                $post['comment'] = count($array_cmt['data']);
                //
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $time1 =  date('Y-m-d H:i:s');
                $time2 =  date("Y-m-d H:i:s", strtotime($post['created_at']));
                $diff = abs(strtotime($time1) - strtotime($time2));
                $years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
                $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
                $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
                $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

                if($diff < 60 and $diff > 0){
                    $time = $seconds." giây";
                }
                if($diff < 3600 and $diff >= 60){
                    $time = $minutes." phút";
                }
                if($diff < 3600*24 and $diff >= 3600){
                    $time = $hours." giờ";
                }
                if($diff >= 3600*24 and $diff < 3600*24*30){
                    $time = $days." ngày";
                }
                if($diff < 3600*24*365 and $diff >= 3600*24*30){
                    $time = $months." tháng";
                }
                if($diff >= 3600*24*365){
                    $time = $years." năm";
                }
                $post['time']=$time;
                $arraytmp = array();
                if (json_decode($post['images']) != null || json_decode($post['images']) != ''){
                    foreach (json_decode($post['images']) as $key => $value) {
                        array_push($arraytmp, $value);
                    }
                }
                $post['images_arr'] = $arraytmp;
                $middle[] = $post;
                $data['data'] =  $middle;
            }
        }
     
        return json_encode($data);
        
    }
    public function poststatus($status,$idClub)
    {
        $data['data'] = [];
        $query_post = Post::select()->where('status', '=', $status)->where('club_id', '=', $idClub);
        $data_post = new Datatable($query_post);
        $data_post->latest();
        $data_post->filterBy([
            'title',
            'content',
            'slug'
        ]);
        $array_posts = $data_post->get()->toArray();

        $links = array(
            'first' => $array_posts ['first_page_url'],
            'last' => $array_posts['last_page_url'],
            'next' => $array_posts ['next_page_url'],
            'prev' =>$array_posts['prev_page_url']
        );
        $meta = array(
            'current_page' => $array_posts['current_page'],
            'from' => $array_posts['from'],
            'last_page' => $array_posts['last_page'],
            'path' =>$array_posts['path'],
            'per_page' => $array_posts['per_page'],
            'to' =>$array_posts['to'],
            'total' =>$array_posts['total']
        );
        //
        $query_user = User::select();
        // $data_user = new Datatable($query_user);
        // $data_user->latest();
        $array_users = $query_user->get()->toArray();

        $query_club = Club::select();
        // $data_club = new Datatable($query_club);
        // $data_club->latest();
        $array_clubs =$query_club->get()->toArray();
       
        //
        if(count($array_posts['data'])>0){
            $data = array();
            $middle = array();
            foreach ($array_posts['data'] as $post) {
                foreach ($array_users as $user) {
                    if($post['user_id']==$user['id']){
                        $post['user']= $user;
                        break;
                    }
                }
                foreach ($array_clubs as $club) {
                    if($post['club_id']==$club['id']){
                        $post['club']= $club;
                        break;
                    }
                }
                //like
                $query_like= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1);
                    
                $data_like = new Datatable($query_like);

                $data_like->latest();
                $data_like->filterBy([
                ]);
                $array_like = $data_like->get()->toArray();
                $post['like'] = count($array_like['data']);
                //comment
                $query_cmt = Comment::select()->where('post_id', '=', $post['id']);;
            
                $data_cmt = new Datatable($query_cmt);

                $data_cmt->latest();
                $data_cmt->filterBy([
                ]);
                $array_cmt = $data_cmt->get()->toArray();
                $post['comment'] = count($array_cmt['data']);
                //
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $time1 =  date('Y-m-d H:i:s');
                $time2 =  date("Y-m-d H:i:s", strtotime($post['created_at']));
                $diff = abs(strtotime($time1) - strtotime($time2));
                $years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
                $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
                $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
                $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

                if($diff < 60 and $diff > 0){
                    $time = $seconds." giây";
                }
                if($diff < 3600 and $diff >= 60){
                    $time = $minutes." phút";
                }
                if($diff < 3600*24 and $diff >= 3600){
                    $time = $hours." giờ";
                }
                if($diff >= 3600*24 and $diff < 3600*24*30){
                    $time = $days." ngày";
                }
                if($diff < 3600*24*365 and $diff >= 3600*24*30){
                    $time = $months." tháng";
                }
                if($diff >= 3600*24*365){
                    $time = $years." năm";
                }
                $post['time']=$time;
                $arraytmp = array();
                if (json_decode($post['images']) != null || json_decode($post['images']) != ''){
                    foreach (json_decode($post['images']) as $key => $value) {
                        array_push($arraytmp, $value);
                    }
                }
                $post['images_arr'] = $arraytmp;
                $middle[] = $post;
                $data['data'] =  $middle;
            }
        }
        $data['meta'] = $meta;
        $data['links'] = $links;
        return json_encode($data);
        
    }
    public function postclublike($idUser,$idClub)
    {
        $data['data'] = [];
        $query_post = Post::select()->where('club_id', '=', $idClub)->where('status','=',1);
        $data_post = new Datatable($query_post);
        $data_post->latest();
        $array_posts = $data_post->get()->toArray();

        $query_user = User::select();
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $array_users = $data_user->get()->toArray();

        $query_club = Club::select();
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $array_clubs = $data_club->get()->toArray();
       
        //
        if(count($array_posts['data'])>0){
            $data = array();
            $middle = array();
            foreach ($array_posts['data'] as $post) {
                foreach ($array_users['data'] as $user) {
                    if($post['user_id']==$user['id']){
                        $post['user']= $user;
                        break;
                    }
                }
                foreach ($array_clubs['data'] as $club) {
                    if($post['club_id']==$club['id']){
                        $post['club']= $club;
                        break;
                    }
                }
                //like
                $query_like= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1);
                    
                $data_like = new Datatable($query_like);

                $data_like->latest();
                $data_like->filterBy([
                ]);
                $array_like = $data_like->get()->toArray();
                $post['like'] = count($array_like['data']);
                //like status
                //like
                $query_likeS= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1)->where('user_id', '=', $idUser);
                    
                $data_likeS = new Datatable($query_likeS);

                $data_likeS->latest();
                $data_likeS->filterBy([
                ]);
                $array_likeS = $data_likeS->get()->toArray();
                if(count($array_likeS['data'])>0){
                    $post['likeStatus'] = $array_likeS['data']['0']['status'];
                    $post['idLike'] = $array_likeS['data']['0']['id'];
                }else{
                    $post['likeStatus'] = null;
                    $post['idLike'] = null;
                }
                //comment
                $query_cmt = Comment::select()->where('post_id', '=', $post['id']);;
            
                $data_cmt = new Datatable($query_cmt);

                $data_cmt->latest();
                $data_cmt->filterBy([
                ]);
                $array_cmt = $data_cmt->get()->toArray();
                $post['comment'] = count($array_cmt['data']);
                //
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $time1 =  date('Y-m-d H:i:s');
                $time2 =  date("Y-m-d H:i:s", strtotime($post['created_at']));
                $diff = abs(strtotime($time1) - strtotime($time2));
                $years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
                $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
                $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
                $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

                if($diff < 60 and $diff > 0){
                    $time = $seconds." giây";
                }
                if($diff < 3600 and $diff >= 60){
                    $time = $minutes." phút";
                }
                if($diff < 3600*24 and $diff >= 3600){
                    $time = $hours." giờ";
                }
                if($diff >= 3600*24 and $diff < 3600*24*30){
                    $time = $days." ngày";
                }
                if($diff < 3600*24*365 and $diff >= 3600*24*30){
                    $time = $months." tháng";
                }
                if($diff >= 3600*24*365){
                    $time = $years." năm";
                }
                $post['time']=$time;
                $arraytmp = array();
                if (json_decode($post['images']) != null || json_decode($post['images']) != ''){
                    foreach (json_decode($post['images']) as $key => $value) {
                        array_push($arraytmp, $value);
                    }
                }
                $post['images_arr'] = $arraytmp;
                $middle[] = $post;
                $data['data'] =  $middle;
            }
        }
     
        return json_encode($data);
        
    }
    public function postclub($idClub)
    {
        $data['data'] = [];
        $query_post = Post::select()->where('club_id', '=', $idClub)->where('status','=',1);
        $data_post = new Datatable($query_post);
        $data_post->latest();
        $array_posts = $data_post->get()->toArray();

        $query_user = User::select();
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $array_users = $data_user->get()->toArray();

        $query_club = Club::select();
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $array_clubs = $data_club->get()->toArray();
       
        //
        if(count($array_posts['data'])>0){
            $data = array();
            $middle = array();
            foreach ($array_posts['data'] as $post) {
                foreach ($array_users['data'] as $user) {
                    if($post['user_id']==$user['id']){
                        $post['user']= $user;
                        break;
                    }
                }
                foreach ($array_clubs['data'] as $club) {
                    if($post['club_id']==$club['id']){
                        $post['club']= $club;
                        break;
                    }
                }
                //like
                $query_like= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1);
                    
                $data_like = new Datatable($query_like);

                $data_like->latest();
                $data_like->filterBy([
                ]);
                $array_like = $data_like->get()->toArray();
                $post['like'] = count($array_like['data']);
                //comment
                $query_cmt = Comment::select()->where('post_id', '=', $post['id']);;
            
                $data_cmt = new Datatable($query_cmt);

                $data_cmt->latest();
                $data_cmt->filterBy([
                ]);
                $array_cmt = $data_cmt->get()->toArray();
                $post['comment'] = count($array_cmt['data']);
                //
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $time1 =  date('Y-m-d H:i:s');
                $time2 =  date("Y-m-d H:i:s", strtotime($post['created_at']));
                $diff = abs(strtotime($time1) - strtotime($time2));
                $years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
                $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
                $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
                $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

                if($diff < 60 and $diff > 0){
                    $time = $seconds." giây";
                }
                if($diff < 3600 and $diff >= 60){
                    $time = $minutes." phút";
                }
                if($diff < 3600*24 and $diff >= 3600){
                    $time = $hours." giờ";
                }
                if($diff >= 3600*24 and $diff < 3600*24*30){
                    $time = $days." ngày";
                }
                if($diff < 3600*24*365 and $diff >= 3600*24*30){
                    $time = $months." tháng";
                }
                if($diff >= 3600*24*365){
                    $time = $years." năm";
                }
                $post['time']=$time;
                $arraytmp = array();
                if (json_decode($post['images']) != null || json_decode($post['images']) != ''){
                    foreach (json_decode($post['images']) as $key => $value) {
                        array_push($arraytmp, $value);
                    }
                }
                $post['images_arr'] = $arraytmp;
                $middle[] = $post;
                $data['data'] =  $middle;
            }
        }
     
        return json_encode($data);
        
    }
    public function postuser($idUser)
    {
        $data['data'] = [];
        $query_post = Post::select()->where('user_id', '=', $idUser)->where('status','=',1);
        $data_post = new Datatable($query_post);
        $data_post->latest();
        $array_posts = $data_post->get()->toArray();

        $query_user = User::select()->where('id', '=', $idUser);
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $array_users = $data_user->get()->toArray();

        $query_club = Club::select();
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $array_clubs = $data_club->get()->toArray();
       
        //
        if(count($array_posts['data'])>0){
            $data = array();
            $middle = array();
            foreach ($array_posts['data'] as $post) {
                foreach ($array_users['data'] as $user) {
                    if($post['user_id']==$user['id']){
                        $post['user']= $user;
                        break;
                    }
                }
                foreach ($array_clubs['data'] as $club) {
                    if($post['club_id']==$club['id']){
                        $post['club']= $club;
                        break;
                    }
                }
                //like
                $query_like= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1);
                    
                $data_like = new Datatable($query_like);

                $data_like->latest();
                $data_like->filterBy([
                ]);
                $array_like = $data_like->get()->toArray();
                $post['like'] = count($array_like['data']);
                //like status
                //like
                $query_likeS= Like::select()->where('post_id', '=', $post['id'])->where('status', '=', 1)->where('user_id', '=', $idUser);
                    
                $data_likeS = new Datatable($query_likeS);

                $data_likeS->latest();
                $data_likeS->filterBy([
                ]);
                $array_likeS = $data_likeS->get()->toArray();
                if(count($array_likeS['data'])>0){
                    $post['likeStatus'] = $array_likeS['data']['0']['status'];
                    $post['idLike'] = $array_likeS['data']['0']['id'];
                }else{
                    $post['likeStatus'] = null;
                    $post['idLike'] = null;
                }
                //comment
                $query_cmt = Comment::select()->where('post_id', '=', $post['id']);;
            
                $data_cmt = new Datatable($query_cmt);

                $data_cmt->latest();
                $data_cmt->filterBy([
                ]);
                $array_cmt = $data_cmt->get()->toArray();
                $post['comment'] = count($array_cmt['data']);
                //
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                $time1 =  date('Y-m-d H:i:s');
                $time2 =  date("Y-m-d H:i:s", strtotime($post['created_at']));
                $diff = abs(strtotime($time1) - strtotime($time2));
                $years = floor($diff / (365*60*60*24));
                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
                $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
                $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
                $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));

                if($diff < 60 and $diff > 0){
                    $time = $seconds." giây";
                }
                if($diff < 3600 and $diff >= 60){
                    $time = $minutes." phút";
                }
                if($diff < 3600*24 and $diff >= 3600){
                    $time = $hours." giờ";
                }
                if($diff >= 3600*24 and $diff < 3600*24*30){
                    $time = $days." ngày";
                }
                if($diff < 3600*24*365 and $diff >= 3600*24*30){
                    $time = $months." tháng";
                }
                if($diff >= 3600*24*365){
                    $time = $years." năm";
                }
                $post['time']=$time;
                $arraytmp = array();
                if (json_decode($post['images']) != null || json_decode($post['images']) != ''){
                    foreach (json_decode($post['images']) as $value) {
                        array_push($arraytmp, $value);
                    }
                }
                $post['images_arr'] = $arraytmp;
                $middle[] = $post;
                $data['data'] =  $middle;
            }
        }
        return json_encode($data);
    }
}