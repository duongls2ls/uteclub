<?php

namespace App\Repositories;

use App\Costactivitydetail;
use App\Costactivity;
use App\Club;
use App\User;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\CostactivitydetailRepositoryInterface;

class CostactivitydetailRepository implements CostactivitydetailRepositoryInterface
{
    public function datatable()
    {
        $query = Costactivitydetail::select();
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return Costactivitydetail::create($data);
    }

    public function details(Costactivitydetail $costactivitydetail)
    {
       return $costactivitydetail;
    }

    public function update(Costactivitydetail $costactivitydetail, $data)
    {
        return $costactivitydetail->update($data);
    }

    public function delete(Costactivitydetail $costactivitydetail)
    {
        return $costactivitydetail->delete();
    }
    public function getbydetail($id)
    {
        $data['data'] = [];
        $query_cost = Costactivitydetail::select()->where('costactivity_id', '=', $id);

        $data_cost = new Datatable($query_cost);

        $data_cost->latest();
        $data_cost->filterBy([
        ]);
        $costs = $data_cost->get()->toArray();
        $links = array(
            'first' => $costs['first_page_url'],
            'last' => $costs['last_page_url'],
            'next' => $costs['next_page_url'],
            'prev' =>$costs['prev_page_url']
        );
        $meta = array(
            'current_page' => $costs['current_page'],
            'from' => $costs['from'],
            'last_page' => $costs['last_page'],
            'path' =>$costs['path'],
            'per_page' => $costs['per_page'],
            'to' =>$costs['to'],
            'total' =>$costs['total']
        );
        
        $middle = [];
        if(count($costs['data'])>0){
            foreach ($costs['data'] as $cost) {
                $query_user = User::select()->where('id', '=', $cost['user_id']);
        
                $data_user = new Datatable($query_user);
        
                $data_user->latest();
                $data_user->filterBy([
                    'name',
                    'email',
                    'mobile_number'
                ]);
                $user = $data_user->get()->toArray();
                //
                if(isset($user['data'][0])){
                    $cost['username'] = $user['data'][0]['name'];
                    $cost['birthday'] =  $user['data'][0]['birthday'];
                    $cost['email'] = $user['data'][0]['email'];
                    $cost['gender'] = $user['data'][0]['gender'];
                    $cost['phone'] =  $user['data'][0]['mobile_number'];
                    $cost['address'] =  $user['data'][0]['address'];
                    $middle[] = $cost;
                }
            }
        }
        $data['data'] = $middle;
        $data['links'] = $links;
        $data['meta'] = $meta;
        
        return json_encode($data);
    }
    public function getbyuser($id)
    {
        $data['data'] = [];
        $query_costd = Costactivitydetail::select()->where('user_id', '=', $id);

        $data_costd = new Datatable($query_costd);

        $data_costd->latest();
        $data_costd->filterBy([
        ]);
        $costds = $data_costd->get()->toArray();
        $links = array(
            'first' => $costds['first_page_url'],
            'last' => $costds['last_page_url'],
            'next' => $costds['next_page_url'],
            'prev' =>$costds['prev_page_url']
        );
        $meta = array(
            'current_page' => $costds['current_page'],
            'from' => $costds['from'],
            'last_page' => $costds['last_page'],
            'path' =>$costds['path'],
            'per_page' => $costds['per_page'],
            'to' =>$costds['to'],
            'total' =>$costds['total']
        );
        $club_arrs = [];
        $middle = [];
        if(count($costds['data'])>0){
            foreach ($costds['data'] as $costd) {
                $query_cost = Costactivity::select()->where('id', '=', $costd['costactivity_id']);
                $data_costs = new Datatable($query_cost);
                $data_costs->latest();
                $data_costs->filterBy([]);
                $costs = $data_costs->get()->toArray();
                //
                $club_arrs[] = $costs['data'][0]['club_id'];
            }
            $club_arrs = array_unique($club_arrs);
            foreach ($club_arrs as $club_arr) {
                $query_club = Club::select([
                    'name',
                    'user_id',
                ])->where('id', '=', $club_arr);
            
                $data_club = new Datatable($query_club);
        
                $data_club->latest();
                $data_club->filterBy([]);
                $club = $data_club->get()->toArray();  
                // 
                $query_user = User::select()->where('id', '=', $club['data'][0]['user_id']);
        
                $data_user = new Datatable($query_user);
        
                $data_user->latest();
                $data_user->filterBy([]);
                $user = $data_user->get()->toArray();
                $club['data'][0]['adminusername'] = $user['data'][0]['name'];
                $club['data'][0]['club_id'] = $club_arr;
                $middle[] = $club['data'][0];
            }
        }
        $club_data = [];
        foreach ($middle as  $mid) {
            $costactivity = [];
            foreach ($costds['data'] as $costd) {
                $query_cost = Costactivity::select()->where('id', '=', $costd['costactivity_id']);
                $data_costs = new Datatable($query_cost);
                $data_costs->latest();
                $data_costs->filterBy([]);
                $costs = $data_costs->get()->toArray();

                if($mid['club_id'] == $costs['data'][0]['club_id']){
                    $costd['cost'] = $costs['data'][0];
                    $costactivity[] = $costd;
                    $mid['costactivity'] =$costactivity;
                }
            }
            $club_data[] = $mid;
        }
        $data['data'] = $club_data;
        $data['links'] = $links;
        $data['meta'] = $meta;
        
        return json_encode($data);
    }
}