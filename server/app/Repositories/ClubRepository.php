<?php

namespace App\Repositories;

use App\Club;
use App\User;
use App\DetailClub;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\ClubRepositoryInterface;

class ClubRepository implements ClubRepositoryInterface
{
    public function datatable()
    {
        $query = Club::select();

        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
            'name',
            'slug',
            'information'
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return Club::create($data);
    }

    public function details(Club $club)
    {
       return $club;
    }

    public function update(Club $club, $data)
    {
        return $club->update($data);
    }

    public function delete(Club $club)
    {
        return $club->delete();
    }
    public function getallClub()
    {
        $query = Club::select();

        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
            'name',
            'slug',
        ]);
        $clubs = $datatable->get()->toArray();
        //
        $links = array(
            'first' => $clubs ['first_page_url'],
            'last' => $clubs['last_page_url'],
            'next' => $clubs ['next_page_url'],
            'prev' =>$clubs['prev_page_url']
        );
        $meta = array(
            'current_page' => $clubs['current_page'],
            'from' => $clubs['from'],
            'last_page' => $clubs['last_page'],
            'path' =>$clubs['path'],
            'per_page' => $clubs['per_page'],
            'to' =>$clubs['to'],
            'total' =>$clubs['total']
        );
        //
        $query_user = User::select();
        $array_users = $query_user->get()->toArray();
        //
        // dd($array_users);
        $data = array();
        $middle = array();
        $data['data'] = [];
        foreach ($clubs['data'] as $club) {
            foreach ($array_users as $user) {
                if($club['user_id']==$user['id']){
                    $club['user'] = $user;
                    $club['name_user'] = $user['name'];
                }
            }
            $query_club = DetailClub::select()
            ->where('club_id', '=', $club['id']);
            $data_club = new Datatable($query_club);
            $data_club->latest();
            $array_clubs = $data_club->get()->toArray();

            $club['count'] = count($array_clubs['data']);
            $middle[] = $club;
            $data['data'] = $middle;
        }
        $data['meta'] = $meta;
        $data['links'] = $links;
        return json_encode($data);
    }
}