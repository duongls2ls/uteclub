<?php

namespace App\Repositories;

use App\ReplyComment;
use App\User;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\ReplyCommentRepositoryInterface;

class ReplyCommentRepository implements ReplyCommentRepositoryInterface
{
    public function datatable()
    {
        $query = ReplyComment::select();
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return ReplyComment::create($data);
    }

    public function details(ReplyComment $replycomment)
    {
       return $replycomment;
    }

    public function update(ReplyComment $replycomment, $data)
    {
        return $replycomment->update($data);
    }

    public function delete(ReplyComment $replycomment)
    {
        return $replycomment->delete();
    }
    public function getreplyByComment($idComnent)
    {
        $query_reply = ReplyComment::select()->where('comment_id', '=', $idComnent);;
        
        $data_reply = new Datatable($query_reply);

        $data_reply->latest();
        $data_reply->filterBy([
        ]);
        $array_reply= $data_reply->get()->toArray();
        //
        $query_user = User::select();
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $data_user->filterBy([
            
        ]);
        $array_users = $data_user->get()->toArray();
        $data = array();
        $middle = array();
        foreach ($array_reply['data'] as $reply) {
            foreach ($array_users['data'] as $user) {
                if($reply['user_id']==$user['id']){
                    $reply['user'] = $user;

                    date_default_timezone_set('Asia/Ho_Chi_Minh');
                    $time1 =  date('Y-m-d H:i:s');
                    $time2 =  date("Y-m-d H:i:s", strtotime($reply['created_at']));
                    $diff = abs(strtotime($time1) - strtotime($time2));

                    $years = floor($diff / (365*60*60*24));
                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
                    $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
                    $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
                    $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
        
                    if($diff < 60 and $diff > 0){
                        $time = $seconds." giây";
                    }
                    if($diff < 3600 and $diff >= 60){
                        $time = $minutes." phút";
                    }
                    if($diff < 3600*24 and $diff >= 3600){
                        $time = $hours." giờ";
                    }
                    if($diff >= 3600*24 and $diff < 3600*24*30){
                        $time = $days." ngày";
                    }
                    if($diff < 3600*24*365 and $diff >= 3600*24*30){
                        $time = $months." tháng";
                    }
                    if($diff >= 3600*24*365){
                        $time = $years." năm";
                    }
                    $reply['time']=$time;


                    $middle[] = $reply;
                    $data['data'] = $middle;
                }
            }
        }
        return json_encode($data);
    
    }
}