<?php

namespace App\Repositories;

use App\Comment;
use App\User;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\CommentRepositoryInterface;

class CommentRepository implements CommentRepositoryInterface
{
    public function datatable()
    {
        $query = Comment::select();
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return Comment::create($data);
    }

    public function details(Comment $comment)
    {
       return $comment;
    }

    public function update(Comment $comment, $data)
    {
        return $comment->update($data);
    }

    public function delete(Comment $comment)
    {
        return $comment->delete();
    }
    public function getcommentByPost($idPost)
    {
        $data['data'] = [];
        $query_cmt = Comment::select()->where('post_id', '=', $idPost);;
        
        $data_cmt = new Datatable($query_cmt);

        $data_cmt->latest();
        $data_cmt->filterBy([
            'content'
        ]);
        $array_cmt = $data_cmt->get()->toArray();

        $links = array(
            'first' => $array_cmt ['first_page_url'],
            'last' => $array_cmt['last_page_url'],
            'next' => $array_cmt ['next_page_url'],
            'prev' =>$array_cmt['prev_page_url']
        );
        $meta = array(
            'current_page' => $array_cmt['current_page'],
            'from' => $array_cmt['from'],
            'last_page' => $array_cmt['last_page'],
            'path' =>$array_cmt['path'],
            'per_page' => $array_cmt['per_page'],
            'to' =>$array_cmt['to'],
            'total' =>$array_cmt['total']
        );
        //
        $query_user = User::select();
        // $data_user = new Datatable($query_user);
        // $data_user->latest();
        // $data_user->filterBy([
        // ]);
        $array_users = $query_user->get()->toArray();
        $middle = array();
        if(count($array_cmt['data'])>0){
            foreach ($array_cmt['data'] as $cmt) {
                foreach ($array_users as $user) {
                    if($cmt['user_id']==$user['id']){
                        $cmt['user'] = $user;
    
                        date_default_timezone_set('Asia/Ho_Chi_Minh');
                        $time1 =  date('Y-m-d H:i:s');
                        $time2 =  date("Y-m-d H:i:s", strtotime($cmt['created_at']));
                        $diff = abs(strtotime($time1) - strtotime($time2));
                        $years = floor($diff / (365*60*60*24));
                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24) / (60*60*24));
                        $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
                        $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60) / 60);
                        $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
    
                        if($diff < 60 and $diff > 0){
                            $time = $seconds." giây";
                        }
                        if($diff < 3600 and $diff >= 60){
                            $time = $minutes." phút";
                        }
                        if($diff < 3600*24 and $diff >= 3600){
                            $time = $hours." giờ";
                        }
                        if($diff >= 3600*24 and $diff < 3600*24*30){
                            $time = $days." ngày";
                        }
                        if($diff < 3600*24*365 and $diff >= 3600*24*30){
                            $time = $months." tháng";
                        }
                        if($diff >= 3600*24*365){
                            $time = $years." năm";
                        }
                        $cmt['time']=$time;
                        $middle[] = $cmt;
                        $data['data'] = $middle;
                    }
                }
            }
        }
        $data['meta'] = $meta;
        $data['links'] = $links;
        return json_encode($data);
    
    }
}