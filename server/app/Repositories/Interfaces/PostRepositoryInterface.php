<?php

namespace App\Repositories\Interfaces;

use App\Post;

interface PostRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(Post $post);

    public function update(Post $post, $data);

    public function delete(Post $post);

    public function getpostByClub($idClub);

    public function postlike($idUser);

    public function postdetaillike($idUser,$idPost);

    public function postsearchlike($idUser,$text);

    public function postsearch($text);

    public function poststatus($status,$idClub);

    public function postclub($idClub);

    public function postclublike($idUser,$idClub);

    public function postuser($idUser);
}