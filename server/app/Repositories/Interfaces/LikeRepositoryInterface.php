<?php

namespace App\Repositories\Interfaces;

use App\Like;

interface LikeRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(Like $Like);

    public function update(Like $Like, $data);

    public function delete(Like $Like);

    public function getLikeByUser($idUser);
}