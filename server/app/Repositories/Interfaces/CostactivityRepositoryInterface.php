<?php

namespace App\Repositories\Interfaces;

use App\Costactivity;

interface CostactivityRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(Costactivity $costactivity);

    public function update(Costactivity $costactivity, $data);

    public function delete(Costactivity $costactivity);

    public function getbyclub($idClub);
}
