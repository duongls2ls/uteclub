<?php

namespace App\Repositories\Interfaces;

use App\PageConnect;

interface PageConnectRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(PageConnect $pageConnect);

    public function update(PageConnect $pageConnect, $data);

    public function delete(PageConnect $pageConnect);
}