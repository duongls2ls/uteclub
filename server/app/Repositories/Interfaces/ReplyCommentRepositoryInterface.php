<?php

namespace App\Repositories\Interfaces;

use App\ReplyComment;

interface ReplyCommentRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(ReplyComment $replycomment);

    public function update(ReplyComment $replycomment, $data);

    public function delete(ReplyComment $replycomment);

    public function getreplyByComment($idComment);
}