<?php

namespace App\Repositories\Interfaces;

use App\Club;

interface ClubRepositoryInterface
{
    public function datatable();

    public function getAllClub();

    public function create($data);

    public function details(Club $club);

    public function update(Club $club, $data);

    public function delete(Club $club);
}