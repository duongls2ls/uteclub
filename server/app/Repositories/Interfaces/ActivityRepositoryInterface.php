<?php

namespace App\Repositories\Interfaces;

use App\Activity;

interface ActivityRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(Activity $activity);

    public function update(Activity $activity, $data);

    public function delete(Activity $activity);

    public function getactByClub($idClub);

    public function getuserByAct($idAct);

    public function getactByClubData($idClub);

    public function getuserByActData($idAct);
    
    public function getactByClubAll();

    public function getActClient($idAct);

    public function getActbyUser($idUser);

    public function getActbyClubUser($idUser);

    public function reportact();
}