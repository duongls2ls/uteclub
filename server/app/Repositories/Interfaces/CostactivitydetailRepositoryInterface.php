<?php

namespace App\Repositories\Interfaces;

use App\Costactivitydetail;

interface CostactivitydetailRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(Costactivitydetail $costactivitydetail);

    public function update(Costactivitydetail $costactivitydetail, $data);

    public function delete(Costactivitydetail $costactivitydetail);

    public function getbydetail($id);

    public function getbyuser($id);
}