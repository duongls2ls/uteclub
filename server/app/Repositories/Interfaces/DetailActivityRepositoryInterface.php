<?php

namespace App\Repositories\Interfaces;

use App\DetailActivity;

interface DetailActivityRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function update(DetailActivity $detailActivity, $data);

    public function details(DetailActivity $detailActivity);

    public function delete(DetailActivity $detailActivity);
}