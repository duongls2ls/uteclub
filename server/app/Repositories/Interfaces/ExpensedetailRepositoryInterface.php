<?php

namespace App\Repositories\Interfaces;

use App\Expensedetail;

interface ExpensedetailRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(Expensedetail $expensedetail);

    public function update(Expensedetail $expensedetail, $data);

    public function delete(Expensedetail $expensedetail);

    public function getbydetail($id);
}