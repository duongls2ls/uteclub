<?php

namespace App\Repositories\Interfaces;

use App\Comment;

interface CommentRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(Comment $comment);

    public function update(Comment $comment, $data);

    public function delete(Comment $comment);

    public function getcommentByPost($idPost);
}