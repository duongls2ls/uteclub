<?php

namespace App\Repositories\Interfaces;

use App\Expense;

interface ExpenseRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(Expense $expense);

    public function update(Expense $expense, $data);

    public function delete(Expense $expense);

    public function getbyclub($idClub);
}