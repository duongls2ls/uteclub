<?php

namespace App\Repositories\Interfaces;

use App\DetailClub;

interface DetailClubRepositoryInterface
{
    public function datatable();

    public function create($data);

    public function details(DetailClub $detailclub);

    public function update(DetailClub $detailclub, $data);

    public function delete(DetailClub $detailclub);

    public function statusclub($idUser,$idPost);

    public function listbyuser($idUser);
}