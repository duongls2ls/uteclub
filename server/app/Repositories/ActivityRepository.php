<?php

namespace App\Repositories;

use App\Activity;
use App\User;
use App\Club;
use App\DetailActivity;
use App\DetailClub;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\ActivityRepositoryInterface;

class ActivityRepository implements ActivityRepositoryInterface
{
    public function datatable()
    {
        $query = Activity::select();
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
            'name',
            'title',
            'content',
            'slug',
            'quantity',
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return Activity::create($data);
    }

    public function details(Activity $activity)
    {
       return $activity;
    }

    public function update(Activity $activity, $data)
    {
        return $activity->update($data);
    }

    public function delete(Activity $comment)
    {
        return $comment->delete();
    }
    public function getactByClub($idClub)
    {
        $query = Activity::select()->where('club_id', '=', $idClub);;
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
            'name',
            'title',
            'content',
            'slug',
            'quantity',
        ]);
        $clubs = $datatable->get()->toArray();
        $data = array();
        $data = $clubs['data'];
        return json_encode($data);
    }
    public function getuserByAct($idAct)
    {
        $query_act = DetailActivity::select()->where('activite_id', '=', $idAct);
        $data_act = new Datatable($query_act);
        $data_act->latest();
        $array_acts = $data_act->get()->toArray();
        // var_dump($data['data']);
        // var_dump(json_encode($data));
        // die;
        $query_user = User::select([
            'id',
            'name',
            'email',
            'mobile_number',
            'birthday',
        ]);
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $data_user->filterBy([
            'id',
            'name',
            'email',
            'mobile_number',
            'birthday',
        ]);
        $array_users = $data_user->get()->toArray();
        $data = array();
        $middle = array();
        foreach ($array_acts['data'] as $act) {
            foreach ($array_users['data'] as $user) {
                if($act['user_id']==$user['id']){
                    $user['status_act'] = $act['status'];
                    $user['act_id'] = $act['activite_id'];
                    $user['created_at'] = $act['created_at'];
                    $middle[] = $user;
                    $data = $middle;
                }
            }
        }
        return json_encode($data);
    }
    public function getactByClubData($idClub)
    {
        $query = Activity::select()->where('club_id', '=', $idClub);;
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
            'name',
            'title',
            'slug',
            'quantity',
        ]);
        return $datatable->get();
    
    }
    public function getuserByActData($idAct)
    {
        $data['data'] = [];
        $query_act = DetailActivity::select()->where('activite_id', '=', $idAct);
        $data_act = new Datatable($query_act);
        $data_act->latest();
        $data_act->filterBy([
            'name',
            'title',
            'content',
            'slug',
            'quantity',
        ]);
        $array_acts = $data_act->get()->toArray();
        $links = array(
            'first' => $array_acts['first_page_url'],
            'last' => $array_acts['last_page_url'],
            'next' => $array_acts['next_page_url'],
            'prev' =>$array_acts['prev_page_url']
        );
        $meta = array(
            'current_page' => $array_acts['current_page'],
            'from' => $array_acts['from'],
            'last_page' => $array_acts['last_page'],
            'path' =>$array_acts['path'],
            'per_page' => $array_acts['per_page'],
            'to' =>$array_acts['to'],
            'total' =>$array_acts['total']
        );
        if(count($array_acts['data'])>0){
            $query_user = User::select([
                'id',
                'name',
                'email',
                'mobile_number',
                'birthday',
            ]);
            // $data_user = new Datatable($query_user);
            // $data_user->latest();
            // $data_user->filterBy([
            //     'id',
            //     'name',
            //     'email',
            //     'mobile_number',
            //     'birthday',
            // ]);
            $array_users = $query_user->get()->toArray();
            $data = array();
            $middle = array();
            foreach ($array_acts['data'] as $act) {
                foreach ($array_users as $user) {
                    if($act['user_id']==$user['id']){
                        $user['status_act'] = $act['status'];
                        $user['act_id'] = $act['activite_id'];
                        $user['created_at'] = $act['created_at'];
                        $user['idDetalAct'] = $act['id'];
                        $middle[] = $user;
                        $data['data'] = $middle;
                    }
                }
            }
        }
        $data['links'] = $links;
        $data['meta'] = $meta;
        return json_encode($data);
    }
    public function getactByClubAll()
    {
        $query_act = Activity::select();
        $data_act = new Datatable($query_act);
        $data_act->latest();
        $data_act->filterBy([
        ]);
        $array_acts = $data_act->get()->toArray();

        // return $datatable->get();
        $query_club = Club::select();
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $data_club->filterBy([]);
        $array_clubs = $data_club->get()->toArray();
        //
        $query_user = User::select([
            'id',
            'name',
            'email',
            'mobile_number',
            'birthday',
        ]);
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $data_user->filterBy([
        ]);
        $array_users = $data_user->get()->toArray();
        //

        $data = array();
        $middle = array();
        $club['activity'] = "";
        foreach ($array_clubs['data'] as $club) {
            $acts = array();
            foreach ($array_acts['data'] as $act) {
                if($club['id']==$act['club_id']){
                    $acts[] = $act;
                }
                $club['activity'] = $acts;
            }
            foreach ($array_users['data'] as $user) {
                if($user['id']==$club['user_id']){
                    $club['user'] = $user;
                    break;
                }
            }
            $middle[] = $club;
            $data['data'] = $middle;
        }
        return json_encode($data);
    }
    public function getActClient($idAct)
    {
        $data['data'] = [];
        $query_act = Activity::select()->where('id', '=', $idAct);
        $data_act = new Datatable($query_act);
        $data_act->latest();
        $data_act->filterBy([
        ]);
        $array_acts = $data_act->get()->toArray();
  
        if(isset($array_acts['data'][0])){
            $array_acts = $data_act->get()->toArray();
            $query_club = Club::select()->where('id', '=', $array_acts['data'][0]['club_id']);
            $data_club = new Datatable($query_club);
            $data_club->latest();
            $data_club->filterBy([]);
            $array_clubs = $data_club->get()->toArray();
            // //
            $query_user = User::select([
                'id',
                'name',
                'email',
                'mobile_number',
                'birthday',
            ])->where('id', '=', $array_clubs['data'][0]['user_id']);
            $data_user = new Datatable($query_user);
            $data_user->latest();
            $data_user->filterBy([
            ]);
            $array_users = $data_user->get()->toArray();

            $data = array();
            $middle = array();
            $middle['activity'] = $array_acts['data'][0];
            $middle['club'] = $array_clubs['data'][0];
            $middle['user'] = $array_users['data'][0];
            $data['data']=$middle;
            
        }
        
        return json_encode($data);
    }
    public function getActbyUser($idUser)
    {
        $data['data'] = [];
        $query_dact = DetailActivity::select()->where('user_id', '=', $idUser);
        $data_dact = new Datatable($query_dact);
        $data_dact->latest();
        $data_dact->filterBy([
        ]);
        $array_dacts = $data_dact->get()->toArray();
        if(count($array_dacts['data'])){
            $mid = array();
        foreach ($array_dacts['data'] as $key => $value) {
            $mid[] = $value['activite_id'];
        }
        //
        $query_act = Activity::select()->whereIn('id',$mid);
        $data_act = new Datatable($query_act);
        $data_act->latest();
        $data_act->filterBy([
        ]);
        $array_acts = $data_act->get()->toArray();

        if(isset($array_acts['data']))
        {
            foreach ($array_acts['data'] as $key => $value) 
            {
                $query_club = Club::select()->where('id', '=', $value['club_id']);
                $data_club = new Datatable($query_club);
                $data_club->latest();
                $data_club->filterBy([]);
                $array_clubs = $data_club->get()->toArray();
                // //
                $query_user = User::select([
                    'id',
                    'name',
                    'email',
                    'mobile_number',
                    'birthday',
                ])->where('id', '=', $array_clubs['data'][0]['user_id']);
                $data_user = new Datatable($query_user);
                $data_user->latest();
                $data_user->filterBy([
                ]);
                $array_users = $data_user->get()->toArray();
                //
                $query_dact = DetailActivity::select()->where('user_id', '=', $idUser)->where('activite_id', '=', $value['id']);
                $data_dact = new Datatable($query_dact);
                $data_dact->latest();
                $data_dact->filterBy([
                ]);
                $array_dacts = $data_dact->get()->toArray();
                //
                $value['username']=$array_users['data'][0]['name'];
                $value['nameclub']=$array_clubs['data'][0]['name'];
               
                $value['statusDAct']=$array_dacts['data'][0]['status'];
                $create = date_create($array_dacts['data'][0]['created_at']);
                $update = date_create($array_dacts['data'][0]['updated_at']);
               
                $value['createDAct_at'] = date_format($create, 'd-m-Y');
                $value['updateDAct_at'] = date_format($update, 'd-m-Y');
                $value['id_DAct'] = $array_dacts['data'][0]['id'];
                $middle[] = $value;
            }
            $data['data'] = $middle;
        }
    }
    return json_encode($data);
    }
    public function getActbyClubUser($idUser)
    {
        $data['data'] = [];
        $query_dclub = DetailClub::select()->where('user_id', '=', $idUser)->where('status', '=',1);
        $data_dclub = new Datatable($query_dclub);
        $data_dclub->latest();
        $data_dclub->filterBy([
        ]);
        $array_dclub = $data_dclub->get()->toArray();
        
        $mid = array();
        foreach ($array_dclub['data'] as $key => $value) {
            $mid[] = $value['club_id'];
        }
        if(count($mid)>0){
            $middles = array();
            foreach ($mid as $key => $value) {
                $middle = array();
                $query_club = Club::select()->where('id', '=', $value);
                $data_club = new Datatable($query_club);
                $data_club->latest();
                $data_club->filterBy([]);
                $array_clubs = $data_club->get()->toArray(); 
                
               
                $query_user = User::select([
                    'id',
                    'name',
                    'email',
                    'mobile_number',
                    'birthday',
                ])->where('id', '=', $array_clubs['data'][0]['user_id']);
                $data_user = new Datatable($query_user);
                $data_user->latest();
                $data_user->filterBy([
                ]);
                $array_users = $data_user->get()->toArray();
                $array_clubs['data'][0]['username'] =  $array_users['data'][0]['name'];
                

                //
                $query_act = Activity::select()->where('club_id',$array_clubs['data'][0]['id']);
                $data_act = new Datatable($query_act);
                $data_act->latest();
                $data_act->filterBy([
                ]);
                $array_acts = $data_act->get()->toArray();
                //
                $middle = array();
                foreach ($array_acts['data'] as $key => $value) {
                    $query_dact = DetailActivity::select()->where('activite_id',$value['id']);
                    $data_dact = new Datatable($query_dact);
                    $data_dact->latest();
                    $data_dact->filterBy([
                    ]);
                    $array_dacts = $data_dact->get()->toArray();
                    $value['count'] = count($array_dacts['data']);

                    $query_dact = DetailActivity::select()->where('activite_id',$value['id'])->where('user_id',$idUser);
                    $data_dact = new Datatable($query_dact);
                    $data_dact->latest();
                    $data_dact->filterBy([
                    ]);
                    $array_dacts = $data_dact->get()->toArray();

                    if(count($array_dacts['data'])>0){ 
                        $value['statusDAct'] = $array_dacts['data'][0]['status'];
                        $value['idDAct'] = $array_dacts['data'][0]['id'];
                    }else{
                        $value['statusDAct'] = null;
                        $value['idDAct'] = null;
                    }
                    $array_dacts = $data_dact->get()->toArray();
                    $middle[] = $value;
                }
                $query_dact = DetailActivity::select()->where('activite_id',$array_acts['data'][0]['id'])->where('status',1);
                $data_dact = new Datatable($query_dact);
                $data_dact->latest();
                $data_dact->filterBy([
                ]);
                $array_acts = $data_act->get()->toArray();
                //
                //
                $array_clubs['data'][0]['activity'] = $middle;
                $middles[] = $array_clubs['data'][0];
            }
            $data['data'] = $middles;
        }
        
        return json_encode($data);
    }
    public function reportact(){
        $data= [];
        $query_act = Activity::select();
        $data_act = new Datatable($query_act);
        $data_act->latest();
        $data_act->filterBy([
        ]);
        $array_acts = $data_act->get()->toArray();

        // return $datatable->get();
        $query_club = Club::select();
        $data_club = new Datatable($query_club);
        $data_club->latest();
        $data_club->filterBy([]);
        $array_clubs = $data_club->get()->toArray();
        //
        $query_user = User::select([
            'id',
            'name',
            'email',
            'mobile_number',
            'birthday',
        ]);
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $data_user->filterBy([
        ]);
        $array_users = $data_user->get()->toArray();
        //

        $data = array();
        $middle = array();
        foreach ($array_acts['data'] as $array_act) {
            foreach ($array_clubs['data'] as $array_club) {
                if($array_club['id']==$array_act['club_id']){
                    foreach ($array_users['data'] as $user) {
                        if($user['id']==$array_club['user_id']){
                            $array_club['nameuser'] = $user['name'];
                            break;
                        }
                    }
                    $array_act['club'] = $array_club;
                }
            }
            $query_dact = DetailActivity::select()->where('activite_id',$array_act['id'])->where('status',1);
            $data_dact = new Datatable($query_dact);
            $data_dact->latest();
            $data_dact->filterBy([
            ]);
            $array_dacts = $data_dact->get()->toArray();
            $array_act['count_user_active'] = count($array_dacts['data']);
            //
            $query_dact = DetailActivity::select()->where('activite_id',$array_act['id'])->where('status',0);
            $data_dact = new Datatable($query_dact);
            $data_dact->latest();
            $data_dact->filterBy([
            ]);
            $array_dacts = $data_dact->get()->toArray();
            $array_act['count_user_noactive'] = count($array_dacts['data']);
            //
            $array_act['count_user'] = $array_act['count_user_noactive']+$array_act['count_user_active'];
            $middle[] = $array_act;
            $data['data'] = $middle;
        }
        return json_encode($data);
    }
}