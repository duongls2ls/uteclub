<?php

namespace App\Repositories;

use App\Expensedetail;
use App\User;
use App\Datatable\Datatable;
use App\Expense;
use App\Repositories\Interfaces\ExpensedetailRepositoryInterface;

class ExpensedetailRepository implements ExpensedetailRepositoryInterface
{
    public function datatable()
    {
        $query = Expensedetail::select();
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return Expensedetail::create($data);
    }

    public function details(Expensedetail $expensedetail)
    {
       return $expensedetail;
    }

    public function update(expensedetail $expensedetail, $data)
    {
        return $expensedetail->update($data);
    }

    public function delete(Expensedetail $expensedetail)
    {
        return $expensedetail->delete();
    }
    public function getbydetail($id)
    {
        $data['data'] = [];
        $query_expense = Expensedetail::select()->where('expense_id', '=', $id);

        $data_expense = new Datatable($query_expense);

        $data_expense->latest();
        $data_expense->filterBy([
            'title',
            'content',
            'amount'
        ]);
        $expenses = $data_expense->get()->toArray();
        $links = array(
            'first' => $expenses['first_page_url'],
            'last' => $expenses['last_page_url'],
            'next' => $expenses['next_page_url'],
            'prev' =>$expenses['prev_page_url']
        );
        $meta = array(
            'current_page' => $expenses['current_page'],
            'from' => $expenses['from'],
            'last_page' => $expenses['last_page'],
            'path' =>$expenses['path'],
            'per_page' => $expenses['per_page'],
            'to' =>$expenses['to'],
            'total' =>$expenses['total']
        );
        
        $middle = [];
        if(count($expenses['data'])>0){
            foreach ($expenses['data'] as $expense) {
                $middle[] = $expense;
            }
        }
        $data['data'] = $middle;
        $data['links'] = $links;
        $data['meta'] = $meta;
        
        return json_encode($data);
    }
}