<?php

namespace App\Repositories;

use App\Client;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\User;
use App\Club;
use App\DetailClub;
use Illuminate\Support\Facades\Auth;

class UserRepository implements UserRepositoryInterface
{
    public function datatable()
    {
        $query = User::select();
        //     [
        //     'id',
        //     'name',
        //     'email',
        //     'mobile_number',
        //     'is_otp_verification_enabled_at_login',
        //     'otp_type',
        //     'created_at'
        // ]
        // )->addSelect([
        //     'last_logged_in_at' => Client::select('logged_in_at')
        //         ->whereColumn('user_id', 'users.id')
        //         ->latest()
        //         ->take(1)
        // ])->where('id', '!=', Auth::id());

        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
            'name',
            'email',
            'mobile_number',
            'gender'
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return User::create($data);
    }

    public function details(User $user)
    {
       return $user;
    }

    public function update(User $user, $data)
    {
        return $user->update($data);
    }

    public function delete(User $user)
    {
        return $user->delete();
    }

    public function updatePassword(User $user, $data)
    {
        $user->password = $data['password'];

        return $user->save();
    }

    public function getuserNotClub()
    {
        $query_club = Club::select();

        $data_club = new Datatable($query_club);

        $data_club->latest();
        $data_club->filterBy([
            'name',
            'user_id'
        ]);
        $clubs = $data_club->get()->toArray();
        $middle = array();
        foreach ($clubs['data'] as $club) {
            $middle[] = $club['user_id'];
        }
        //
        $query_user = User::select([
            'id',
            'name',
            'email',
            'mobile_number',
            'birthday',
        ])->whereNotIn('id', $middle);
        $data_user = new Datatable($query_user);
        $data_user->latest();
        $data_user->filterBy([
            'id',
            'name',
            'email',
            'mobile_number',
            'birthday',
        ]);
        $array_users = $data_user->get()->toArray();
        //
        $data = array();
        $data['data'] = $array_users['data'];
        
        return json_encode($data);
    }
    public function getuserByClub0($idClub)
    {
        $query_club = DetailClub::select()->where('club_id', '=', $idClub)->where('status', '=',0 );

        $data_club = new Datatable($query_club);

        $data_club->latest();
        $data_club->filterBy([]);
        $clubs = $data_club->get()->toArray();

        $links = array(
            'first' => $clubs['first_page_url'],
            'last' => $clubs['last_page_url'],
            'next' => $clubs['next_page_url'],
            'prev' =>$clubs['prev_page_url']
        );
        $meta = array(
            'current_page' => $clubs['current_page'],
            'from' => $clubs['from'],
            'last_page' => $clubs['last_page'],
            'path' =>$clubs['path'],
            'per_page' => $clubs['per_page'],
            'to' =>$clubs['to'],
            'total' =>$clubs['total']
        );
        
        $data['data'] = [];
        if(count($clubs['data'])>0){
            $middle = array();
            foreach ($clubs['data'] as $club) {
                $middle[] = $club['user_id'];
            }
            //
            //
            $query_user = User::select([
                'id',
                'name',
                'email',
                'mobile_number',
                'birthday',
            ])->whereIn('id', $middle);
            $data_user = new Datatable($query_user);
            $data_user->latest();
            $data_user->filterBy([
                'name',
                'email',
                'mobile_number',
                'birthday',
            ]);
            $users = $data_user->get()->toArray();
            $middle = array();
            foreach ($clubs['data'] as $club) {
                foreach ($users['data'] as $user) {
                    if($club['user_id']==$user['id']){
                        $user['id_detail'] = $club['id'];
                        $middle[] = $user;
                        $data['data'] = $middle;
                    }
                }
            }
        }
        $data['links'] = $links;
        $data['meta'] = $meta;
        return json_encode($data);
    }
    public function getuserByClub1($idClub)
    {
        $data['data'] = [];
        $query_club = DetailClub::select()->where('club_id', '=', $idClub)->where('status', '=',1 );

        $data_club = new Datatable($query_club);

        $data_club->latest();
        $data_club->filterBy([
        ]);
        $clubs = $data_club->get()->toArray();
        $links = array(
            'first' => $clubs['first_page_url'],
            'last' => $clubs['last_page_url'],
            'next' => $clubs['next_page_url'],
            'prev' =>$clubs['prev_page_url']
        );
        $meta = array(
            'current_page' => $clubs['current_page'],
            'from' => $clubs['from'],
            'last_page' => $clubs['last_page'],
            'path' =>$clubs['path'],
            'per_page' => $clubs['per_page'],
            'to' =>$clubs['to'],
            'total' =>$clubs['total']
        );
        if(count($clubs['data'])>0){
            $middle = array();
            foreach ($clubs['data'] as $club) {
                $middle[] = $club['user_id'];
            }
            //
            //
            $query_user = User::select([
                'id',
                'name',
                'email',
                'mobile_number',
                'birthday',
            ])->whereIn('id', $middle);
            $data_user = new Datatable($query_user);
            $data_user->latest();
            $data_user->filterBy([
                'id',
                'name',
                'email',
                'mobile_number',
                'birthday',
            ]);
            $users = $data_user->get()->toArray();
            $middle = array();
            foreach ($clubs['data'] as $club) {
                foreach ($users['data'] as $user) {
                    if($club['user_id']==$user['id']){
                        $user['id_detail'] = $club['id'];
                        $middle[] = $user;
                        $data['data'] = $middle;
                    }
                }
            }
        }
        $data['links'] = $links;
        $data['meta'] = $meta;
        
        return json_encode($data);
    }
}