<?php

namespace App\Repositories;

use App\Like;
use App\User;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\LikeRepositoryInterface;

class LikeRepository implements LikeRepositoryInterface
{
    public function datatable()
    {
        $query = Like::select();
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return Like::create($data);
    }

    public function details(Like $Like)
    {
       return $Like;
    }

    public function update(Like $Like, $data)
    {
        return $Like->update($data);
    }

    public function delete(Like $Like)
    {
        return $Like->delete();
    }
    public function getLikeByUser($idUser)
    {
        $query = Like::select()->where('user_id', '=', $idUser)->where('status', '=', 1);
        
        $data= new Datatable($query);

        $data->latest();
        $data->filterBy([
        ]);

        return $data->get();
    
    }
}