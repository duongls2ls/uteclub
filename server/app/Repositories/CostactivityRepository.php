<?php

namespace App\Repositories;

use App\Costactivity;
use App\DetailClub;
use App\Costactivitydetail;
use App\User;
use App\Club;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\CostactivityRepositoryInterface;

class CostactivityRepository implements CostactivityRepositoryInterface
{
    public function datatable()
    {
        $query = Costactivity::select();
        
        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return Costactivity::create($data);
    }

    public function details(Costactivity $costactivity)
    {
       return $costactivity;
    }

    public function update(Costactivity $costactivity, $data)
    {
        return $costactivity->update($data);
    }

    public function delete(Costactivity $costactivity)
    {
        return $costactivity->delete();
    }
    public function getbyclub($idClub)
    {
        $data['data'] = [];
        $query_cost = Costactivity::select()->where('club_id', '=', $idClub);

        $data_cost = new Datatable($query_cost);

        $data_cost->latest();
        $data_cost->filterBy([
            'title',
            'content',
            'quota'
        ]);
        $costs = $data_cost->get()->toArray();
        $links = array(
            'first' => $costs['first_page_url'],
            'last' => $costs['last_page_url'],
            'next' => $costs['next_page_url'],
            'prev' =>$costs['prev_page_url']
        );
        $meta = array(
            'current_page' => $costs['current_page'],
            'from' => $costs['from'],
            'last_page' => $costs['last_page'],
            'path' =>$costs['path'],
            'per_page' => $costs['per_page'],
            'to' =>$costs['to'],
            'total' =>$costs['total']
        );
        $query_club = Club::select([
            'id',
            'name'
        ])->where('id', '=', $idClub);

        $data_club = new Datatable($query_club);

        $data_club->latest();
        $data_club->filterBy([
            'id',
            'name',
        ]);
        $club = $data_club->get()->toArray();
        $middle = [];
        if(count($costs['data'])>0){
            foreach ($costs['data'] as $cost) {
                $cost['club'] = $club['data'];
                $middle[] = $cost;
            }
        }
        $data['data'] = $middle;
        $data['links'] = $links;
        $data['meta'] = $meta;
        
        return json_encode($data);
    }
}