<?php

namespace App\Repositories;

use App\DetailClub;
use App\Club;
use App\Datatable\Datatable;
use App\Repositories\Interfaces\DetailClubRepositoryInterface;

class DetailClubRepository implements DetailClubRepositoryInterface
{
    public function datatable()
    {
        $query = DetailClub::select();

        $datatable = new Datatable($query);

        $datatable->latest();
        $datatable->filterBy([
        ]);

        return $datatable->get();
    }

    public function create($data)
    {
        return DetailClub::create($data);
    }

    public function details(DetailClub $detailclub)
    {
       return $detailclub;
    }

    public function update(DetailClub $detailclub, $data)
    {
        return $detailclub->update($data);
    }

    public function delete(DetailClub $detailclub)
    {
        return $detailclub->delete();
    }
    public function statusclub($idUser,$idClub)
    {
        $data['data'] = [];
        $query_club = DetailClub::select([
            'id',
            'status'
        ])->where('club_id', '=', $idClub)->where('user_id', '=',$idUser );

        $data_club = new Datatable($query_club);

        $data_club->latest();
        $data_club->filterBy([
            'id',
            'status'
        ]);
        $clubs = $data_club->get()->toArray();
        if(count($clubs['data'])>0){
            $data['data'] = $clubs['data'][0];
        }else{
            $clubs['data']['id'] = null;
            $clubs['data']['status'] = -1;
            $data['data'] = $clubs['data'];
        }
        return json_encode($data);
    }
    public function listbyuser($idUser)
    {
        $data['data'] = [];
        $query_dclub = DetailClub::select()->where('user_id', '=',$idUser )->where('status', '=',1 );

        $data_dclub = new Datatable($query_dclub);

        $data_dclub->latest();
        $data_dclub->filterBy([
        ]);
        $dclubs = $data_dclub->get()->toArray();
        $middle = array();
        foreach ($dclubs['data'] as $value) {
            $middle[] = $value['club_id'];
        }
        
        $query_club = Club::select([
            'id',
            'name'
        ])->whereIn('id',$middle );

        $data_club = new Datatable($query_club);

        $data_club->latest();
        $data_club->filterBy([
        ]);
        $clubs = $data_club->get()->toArray();

        if(count($clubs['data'])>0){
            $data = $clubs['data'];
        }
        return json_encode($data);
    }
}