<?php

namespace App\Providers;

use App\Repositories\Interfaces\ClientRepositoryInterface;
use App\Repositories\Interfaces\DetailActivityRepositoryInterface;
use App\Repositories\Interfaces\PageConnectRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\ClubRepositoryInterface;
use App\Repositories\Interfaces\DetailClubRepositoryInterface;
use App\Repositories\Interfaces\PostRepositoryInterface;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use App\Repositories\Interfaces\ReplyCommentRepositoryInterface;
use App\Repositories\Interfaces\ActivityRepositoryInterface;
use App\Repositories\Interfaces\LikeRepositoryInterface;
use App\Repositories\Interfaces\CostactivityRepositoryInterface;
use App\Repositories\Interfaces\CostactivitydetailRepositoryInterface;
use App\Repositories\Interfaces\ExpenseRepositoryInterface;
use App\Repositories\Interfaces\ExpensedetailRepositoryInterface;
use App\Repositories\ClubRepository;
use App\Repositories\DetailClubRepository;
use App\Repositories\ActivityRepository;
use App\Repositories\DetailActivityRepository;
use App\Repositories\PageConnectRepository;
use App\Repositories\UserRepository;
use App\Repositories\ClientRepository;
use App\Repositories\PostRepository;
use App\Repositories\CommentRepository;
use App\Repositories\ReplyCommentRepository;
use App\Repositories\LikeRepository;
use App\Repositories\CostactivityRepository;
use App\Repositories\CostactivitydetailRepository;
use App\Repositories\ExpenseRepository;
use App\Repositories\ExpensedetailRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public $bindings = [
        UserRepositoryInterface::class => UserRepository::class,
        PageConnectRepositoryInterface::class => PageConnectRepository::class,
        ClientRepositoryInterface::class => ClientRepository::class,
        ClubRepositoryInterface::class => ClubRepository::class,
        PostRepositoryInterface::class => PostRepository::class,
        CommentRepositoryInterface::class => CommentRepository::class,
        ActivityRepositoryInterface::class => ActivityRepository::class,
        DetailActivityRepositoryInterface::class => DetailActivityRepository::class,
        ReplyCommentRepositoryInterface::class => ReplyCommentRepository::class,
        DetailClubRepositoryInterface::class => DetailClubRepository::class,
        LikeRepositoryInterface::class => LikeRepository::class,
        CostactivityRepositoryInterface::class => CostactivityRepository::class,
        CostactivitydetailRepositoryInterface::class => CostactivitydetailRepository::class,
        ExpenseRepositoryInterface::class => ExpenseRepository::class,
        ExpensedetailRepositoryInterface::class => ExpensedetailRepository::class
    ];
}
