<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::resource('users', 'UserController')
//                 ->except(['create', 'edit']);
//route Error
Route::get('/error', function(){
    return view('error');
})->name('error');;

//
Route::resource('users', 'UserController');
Route::resource('detailactivitys', 'DetailActivityController');
Route::resource('clubs', 'ClubController');
Route::resource('detailclubs', 'DetailClubController');
Route::resource('posts', 'PostController');
Route::resource('comments', 'CommentController');
Route::resource('replycomments', 'ReplyCommentController');
Route::resource('activities', 'ActivityController');
Route::resource('pageconnects', 'PageConnectController');
Route::resource('likes', 'LikeController');
Route::resource('costactivity', 'CostactivityController');
Route::resource('costactivitydetail', 'CostactivitydetailController');
Route::resource('expense', 'ExpenseController');
Route::resource('expensedetail', 'ExpensedetailController');


Route::post('/users/login', 'UserController@Login');
Route::post('/users/logout', 'UserController@Logout');

Route::get('/activitie/getactbyclub/{club_id}', 'ActivityController@getActByClub');
Route::get('/activitie/getuserbyact/{activite_id}', 'ActivityController@getUserByAct');
Route::get('/activitie/getactclient/{isAct}', 'ActivityController@getActClient');
//
Route::get('/activitie/getactbyclubdata/{club_id}', 'ActivityController@getActByClubData');
Route::get('/activitie/getuserbyactdata/{activite_id}', 'ActivityController@getUserByActData');
Route::get('/activitie/getuserbyactall', 'ActivityController@getUserByActAll');
Route::get('/activitie/getactbyuser/{user_id}', 'ActivityController@getActbyUser');
Route::get('/activitie/getactbyclubuser/{user_id}', 'ActivityController@getActbyClubUser');
Route::get('/activitie/reportact/', 'ActivityController@reportact');
//
Route::get('/post/getpostbyclub/{club_id}', 'PostController@getPostByClub');
Route::get('/club/getallclub', 'ClubController@getAll');
//
Route::get('/comment/getreply/{comment_id}', 'ReplyCommentController@getReplyByComment');
Route::get('/comment/getcmtbypost/{post_id}', 'CommentController@getCommentByPost');
//
Route::get('/like/getlikebyuser/{user_id}', 'LikeController@getLikeByUser');
///
Route::get('/postsearchlike/{user_id}/{text}', 'PostController@postsearchlike');
Route::get('/postsearch/{text}', 'PostController@postsearch');
//
///
Route::get('/postclublike/{user_id}/{club_id}', 'PostController@postclublike');
Route::get('/postclub/{club_id}', 'PostController@postclub');
//
Route::get('/postuser/{user_id}', 'PostController@postuser');
//
Route::get('/postlike/{user_id}', 'PostController@postlike');
Route::get('/postdetaillike/{user_id}/{post_id}/', 'PostController@postdetaillike');
//
//
Route::get('/poststatus/{status}/{club_id}', 'PostController@poststatus');
//
Route::get('/statusclub/{user_id}/{club_id}/', 'DetailClubController@StatusClub');
//
Route::get('/listclubbyuser/{club_id}', 'DetailClubController@listbyuser');
//
Route::get('costactivity/getbyclub/{id}', 'CostactivityController@getbyclub');
Route::get('costactivitydetail/getbydetail/{id}', 'CostactivitydetailController@getbydetail');
Route::get('costactivitydetail/getbyuser/{id}', 'CostactivitydetailController@getbyuser');
//
Route::get('expenses/getbyclub/{id}', 'ExpenseController@getbyclub');
Route::get('expensedetail/getbydetail/{id}', 'ExpensedetailController@getbydetail');
//
Route::get('/user/getusernotclub', 'UserController@getUserNotClub');
Route::get('/user/getuserbyclub0/{club_id}', 'UserController@getUserByClub0');
Route::get('/user/getuserbyclub1/{club_id}', 'UserController@getUserByClub1');

Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => 'fingerprint-header-required'], function () {
    Route::post('/login', 'Auth\LoginController@login');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('/auth/check', 'Auth\LoginController@check');

        Route::group(['middleware' => 'verify-otp-at-login'], function () {
            Route::group(['middleware' => 'google2fa-is-not-activated'], function () {
                Route::post('/checkpoint', 'Auth\CheckpointController@check');
                Route::get('/checkpoint/resend', 'Auth\CheckpointController@resend');
            });

            Route::get('/checkpoint/google2fa/activate', 'Auth\CheckpointController@activateGoogle2fa')
                ->middleware('activate-google2fa');
        });

        Route::post('/logout', 'Auth\LoginController@logout');

        Route::group(['middleware' => 'access-app'], function () {
            Route::get('/dashboard', function (Request $request) {
                return $request->user();
            });

            Route::patch('/settings/user', 'UserController@updateMe');
            Route::patch('/settings/user/password', 'UserController@updateMyPassword');
            Route::patch('/settings/user/pin', 'UserController@updateMyPin');

            Route::patch('/users/{user}/password', 'UserController@updatePassword');
            Route::patch('/users/{user}/pin', 'UserController@updatePin');
            // Route::resource('users', 'UserController')
            //     ->except(['create', 'edit']);
            // //
            // Route::resource('clubs', 'ClubController');
            //
            Route::patch('/clients/{client}/enabled', 'ClientController@changeEnabledStatus');
            Route::delete('/clients/{client}', 'ClientController@destroy');
            Route::get('/clients', 'ClientController@index');
            //
            // Route::resource('/clubs', 'ClubController');
        });
    });
});