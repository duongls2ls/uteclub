/*
 Navicat Premium Data Transfer

 Source Server         : DXD
 Source Server Type    : MySQL
 Source Server Version : 100417
 Source Host           : localhost:3306
 Source Schema         : dxd_vue

 Target Server Type    : MySQL
 Target Server Version : 100417
 File Encoding         : 65001

 Date: 21/04/2021 00:23:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for activities
-- ----------------------------
DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `quantity` int(11) NULL DEFAULT NULL,
  `club_id` int(11) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '1. Mở đăng ký\r\n2. Đóng đăng ký\r\n3. Đang diễn ra\r\n4. Đã diễn ra\r\n',
  `date_start` datetime(0) NULL DEFAULT NULL,
  `date_end` datetime(0) NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of activities
-- ----------------------------
INSERT INTO `activities` VALUES (1, 'dxd', '1', '1', 10, 1, 1, '2021-04-20 23:10:57', '2021-04-20 23:11:02', '1', '2021-04-20 23:11:06', '2021-04-20 23:11:09');

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `fingerprint` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `client` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `platform` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_otp_verified_at_login` tinyint(1) NOT NULL DEFAULT 0,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 1,
  `logged_in_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `clients_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `clients_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clients
-- ----------------------------
INSERT INTO `clients` VALUES (1, 12, 'UrcwWQmwMR', 'Mozilla/5.0 (X11; Linux i686; rv:5.0) Gecko/20130604 Firefox/37.0', 'Unknown', '76.149.176.105', 0, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `clients` VALUES (2, 12, 'GVRwwvWiKg', 'Mozilla/5.0 (Windows 95) AppleWebKit/5351 (KHTML, like Gecko) Chrome/40.0.875.0 Mobile Safari/5351', 'Unknown', '35.60.166.14', 0, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `clients` VALUES (3, 12, '2qAMI4cotE', 'Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 5.1; Trident/3.1)', 'Unknown', '23.41.29.92', 0, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `clients` VALUES (4, 12, 'vGFR7D81eQ', 'Mozilla/5.0 (X11; Linux i686; rv:5.0) Gecko/20111016 Firefox/36.0', 'Unknown', '220.105.151.15', 0, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `clients` VALUES (5, 12, 'fkCLNkPQnU', 'Mozilla/5.0 (X11; Linux x86_64; rv:7.0) Gecko/20191001 Firefox/37.0', 'Unknown', '96.49.136.194', 0, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `clients` VALUES (6, 12, 'TVFZuWrGRq', 'Mozilla/5.0 (compatible; MSIE 11.0; Windows NT 6.2; Trident/4.1)', 'Unknown', '82.221.162.65', 0, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `clients` VALUES (7, 12, 'ESq0Oq9MIj', 'Opera/9.57 (X11; Linux i686; en-US) Presto/2.8.220 Version/10.00', 'Unknown', '136.60.170.175', 0, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `clients` VALUES (8, 12, 'APrsOovzB6', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3) AppleWebKit/5322 (KHTML, like Gecko) Chrome/37.0.857.0 Mobile Safari/5322', 'Unknown', '196.70.78.151', 0, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `clients` VALUES (9, 12, 'df95zlh3op', 'Mozilla/5.0 (compatible; MSIE 5.0; Windows NT 6.1; Trident/5.1)', 'Unknown', '45.72.1.169', 0, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `clients` VALUES (10, 12, 'TNOFjhStFL', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 5.1; Trident/5.1)', 'Unknown', '129.109.59.48', 0, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `clients` VALUES (11, 1, '27c415bc3168d6866f8569cfa97d7ccf', 'Chrome 89.0.4389', 'Windows 10', '127.0.0.1', 0, 1, '2021-03-15 07:49:25', '2021-03-14 01:18:05', '2021-03-15 07:49:25');
INSERT INTO `clients` VALUES (13, 1, 'abbaf08da3bddbbe4c2d70523fda80e9', 'Chrome 89.0.4389', 'Windows 10', '127.0.0.1', 0, 1, '2021-03-28 15:33:36', '2021-03-17 14:10:33', '2021-03-28 15:33:36');
INSERT INTO `clients` VALUES (14, 1, 'e292f9266221e94b768b6daaf1ad8e14', 'Chrome Mobile 89.0.4389', 'Android 6.0.1', '127.0.0.1', 0, 1, '2021-03-25 15:27:38', '2021-03-25 15:23:15', '2021-03-25 15:27:38');
INSERT INTO `clients` VALUES (16, 1, '7744bb4cb061a153adcb71410542118c', 'Chrome 89.0.4389', 'Windows 10', '127.0.0.1', 0, 1, '2021-03-25 15:57:23', '2021-03-25 15:57:23', '2021-03-25 15:57:23');
INSERT INTO `clients` VALUES (17, 1, '2d8441a441b660aeb496654fb6bf30d4', 'Chrome 89.0.4389', 'Windows 10', '127.0.0.1', 0, 1, '2021-03-26 07:21:00', '2021-03-26 07:21:00', '2021-03-26 07:21:00');
INSERT INTO `clients` VALUES (18, 1, '2fdc92bb36440f1064466cbca8817b7c', 'Chrome 89.0.4389', 'Windows 10', '127.0.0.1', 0, 1, '2021-04-14 04:29:47', '2021-04-02 04:58:15', '2021-04-14 04:29:47');
INSERT INTO `clients` VALUES (19, 1, '3a5dd2fe247fc9fe730fab3280600291', 'Chrome 89.0.4389', 'Windows 10', '127.0.0.1', 0, 1, '2021-04-16 07:22:18', '2021-04-16 07:21:59', '2021-04-16 07:22:18');
INSERT INTO `clients` VALUES (20, 1, '25e0adfee209d5929faaf4c6516ecd0f', 'Chrome 89.0.4389', 'Windows 10', '127.0.0.1', 0, 1, '2021-04-20 13:00:38', '2021-04-16 07:22:37', '2021-04-20 13:00:38');

-- ----------------------------
-- Table structure for clubs
-- ----------------------------
DROP TABLE IF EXISTS `clubs`;
CREATE TABLE `clubs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `Infomation` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `banner` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clubs
-- ----------------------------
INSERT INTO `clubs` VALUES (1, 'LB', NULL, 1, 'sbc', '1', '2021-03-09 23:47:25', '2021-03-09 23:47:27');

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` int(11) NOT NULL,
  `facebook` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `instagram` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `youtube` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for detail_activities
-- ----------------------------
DROP TABLE IF EXISTS `detail_activities`;
CREATE TABLE `detail_activities`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activite_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `status` int(255) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of detail_activities
-- ----------------------------
INSERT INTO `detail_activities` VALUES (1, 1, 1, 0, '2021-04-20 08:26:28', '2021-04-20 08:26:30');
INSERT INTO `detail_activities` VALUES (2, NULL, NULL, NULL, '2021-04-20 01:42:37', '2021-04-20 01:42:37');
INSERT INTO `detail_activities` VALUES (3, 1, 1, 0, '2021-04-20 01:45:19', '2021-04-20 01:45:19');

-- ----------------------------
-- Table structure for detail_clubs
-- ----------------------------
DROP TABLE IF EXISTS `detail_clubs`;
CREATE TABLE `detail_clubs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `status` tinyint(11) NULL DEFAULT NULL COMMENT '1. Hoạt động\r\n\r\n2. Bị khóa\r\n\r\n3. Ngưng hoạt động',
  `create_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of detail_clubs
-- ----------------------------
INSERT INTO `detail_clubs` VALUES (1, 1, 1, 2, NULL, NULL);

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED NULL DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jobs_queue_index`(`queue`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (18, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (19, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (20, '2016_06_01_000001_create_oauth_auth_codes_table', 1);
INSERT INTO `migrations` VALUES (21, '2016_06_01_000002_create_oauth_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (22, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1);
INSERT INTO `migrations` VALUES (23, '2016_06_01_000004_create_oauth_clients_table', 1);
INSERT INTO `migrations` VALUES (24, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);
INSERT INTO `migrations` VALUES (25, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (26, '2020_06_02_102644_create_clients_table', 1);
INSERT INTO `migrations` VALUES (27, '2020_06_02_110249_add_client_lock_columns_to_users_table', 1);
INSERT INTO `migrations` VALUES (28, '2020_06_02_131734_add_ip_lock_cloumn_to_users_table', 1);
INSERT INTO `migrations` VALUES (29, '2020_06_05_171051_create_jobs_table', 1);
INSERT INTO `migrations` VALUES (30, '2020_06_21_181143_add_otp_columns_to_users_table', 1);
INSERT INTO `migrations` VALUES (31, '2020_06_24_153241_add_is_otp_verified_at_login_column_to_clients_table', 1);
INSERT INTO `migrations` VALUES (32, '2020_08_09_025443_add_is_active_column_to_users_table', 1);
INSERT INTO `migrations` VALUES (33, '2021_03_12_015202_create_permissions_table', 1);
INSERT INTO `migrations` VALUES (34, '2021_03_12_015319_create_roles_table', 1);

-- ----------------------------
-- Table structure for oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_access_tokens_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_access_tokens
-- ----------------------------
INSERT INTO `oauth_access_tokens` VALUES ('11c65468d472de7aeb3d36449fb3290a689fb4e365cd6edf467c203a135792051737fddd6dd8792a', 1, 1, 'Laravel', '[]', 0, '2021-04-03 01:05:03', '2021-04-03 01:05:03', '2021-04-10 01:05:00');
INSERT INTO `oauth_access_tokens` VALUES ('1513bce85daf93d40863c799d0ccbe3e40b10dffa79fe7c5ee58cde41b15d4e4d5285eb6e0ccda87', 1, 1, 'Laravel', '[]', 0, '2021-04-03 14:08:57', '2021-04-03 14:08:57', '2021-04-10 14:08:54');
INSERT INTO `oauth_access_tokens` VALUES ('1e8b0506a77706d5d3b4cf910f73be5844c95eb369b06cce76bf4eafc0146b63093d9a89b02bde34', 1, 1, 'Laravel', '[]', 0, '2021-04-16 07:22:00', '2021-04-16 07:22:00', '2021-04-23 07:21:59');
INSERT INTO `oauth_access_tokens` VALUES ('228342513b17cb8922d2393e60707606e59b2639a89ba26d243c5c677f75607da760ab2a6bf73bec', 1, 1, 'Laravel', '[]', 0, '2021-04-17 15:26:38', '2021-04-17 15:26:38', '2021-04-24 15:26:37');
INSERT INTO `oauth_access_tokens` VALUES ('2cc62fa9ecbf55f3da86b02be024f3a3cb24f936ae265403cbc9b450b58410bba050804b13d8d941', 1, 1, 'Laravel', '[]', 0, '2021-03-26 07:21:01', '2021-03-26 07:21:01', '2021-04-02 07:20:58');
INSERT INTO `oauth_access_tokens` VALUES ('44201bd4331cdc0bc60c2429ffd55c8d83ce09f5364fa532874dac85510f35f3d91d661fa8a63a98', 1, 1, 'Laravel', '[]', 0, '2021-04-18 11:00:52', '2021-04-18 11:00:52', '2021-04-25 11:00:48');
INSERT INTO `oauth_access_tokens` VALUES ('4b37980c8d99ee9ad104d4ccd8b3fe9e30ea2688d0957e32a6aa48a82b16a6a0e1460058ed7cae83', 1, 1, 'Laravel', '[]', 0, '2021-04-16 07:22:37', '2021-04-16 07:22:37', '2021-04-23 07:22:37');
INSERT INTO `oauth_access_tokens` VALUES ('4e418dcd972601552d8481e7ac8d282e49d2bff4091d794c7cae02a886bc947c7f378b3f974f1e0a', 1, 1, 'Laravel', '[]', 0, '2021-04-09 03:30:53', '2021-04-09 03:30:53', '2021-04-16 03:30:51');
INSERT INTO `oauth_access_tokens` VALUES ('515ed02828e43cc7765713381f756d7c4c52b86398c8bdda0577f1efb665dd79c852f707457dd1d2', 1, 1, 'Laravel', '[]', 0, '2021-04-17 14:37:42', '2021-04-17 14:37:42', '2021-04-24 14:37:42');
INSERT INTO `oauth_access_tokens` VALUES ('5c2d0655e97f1d0762742ac826c98b8b9d53fddab290a204bd5afc2babbb53d5eb6be16002e78c02', 1, 1, 'Laravel', '[]', 0, '2021-04-20 13:00:39', '2021-04-20 13:00:39', '2021-04-27 13:00:36');
INSERT INTO `oauth_access_tokens` VALUES ('5cbdf10098c39271c2a88db4f2835ed396d21cedc06a9be23c2280ce827e49421ace5a13dea5ded8', 1, 1, 'Laravel', '[]', 0, '2021-04-19 02:07:59', '2021-04-19 02:07:59', '2021-04-26 02:07:57');
INSERT INTO `oauth_access_tokens` VALUES ('74d22b7fcebebc36b4a49c1069136784494ed81c216f1d925bd0ee4038bf0553fd68f1d5547b2a7c', 1, 1, 'Laravel', '[]', 0, '2021-03-25 15:57:23', '2021-03-25 15:57:23', '2021-04-01 15:57:23');
INSERT INTO `oauth_access_tokens` VALUES ('808fc4139a52f03d31072f929d7b3df743ffb1ba3a256696b9e1081876f98bc0e57c7531bc269d7e', 1, 1, 'Laravel', '[]', 0, '2021-04-16 07:35:31', '2021-04-16 07:35:31', '2021-04-23 07:35:31');
INSERT INTO `oauth_access_tokens` VALUES ('873a74d8e26b8c7b69b76300b232fc57ed0642aa755b2055250a5692efb1d8cb7398db230770ee70', 1, 1, 'Laravel', '[]', 0, '2021-04-17 14:16:04', '2021-04-17 14:16:04', '2021-04-24 14:16:00');
INSERT INTO `oauth_access_tokens` VALUES ('8a604ed692861b3c0af08e72f45c748a4bc21a14a86561107cbbd91c19e38fe07eafdeccff4edbd8', 1, 1, 'Laravel', '[]', 0, '2021-04-10 04:10:35', '2021-04-10 04:10:35', '2021-04-17 04:10:34');
INSERT INTO `oauth_access_tokens` VALUES ('8aa8ad0432bee09860dd8d81d90a0cb91d27db5acf39500b3a47d131e50fd32d46ab0a319b17b6de', 1, 1, 'Laravel', '[]', 0, '2021-04-16 07:22:18', '2021-04-16 07:22:18', '2021-04-23 07:22:17');
INSERT INTO `oauth_access_tokens` VALUES ('94c06eb643ec737fff0a6863a67fb9f5f5b96ffcbb1b7d041a249043c1fbd5df187364370d2b776b', 1, 1, 'Laravel', '[]', 0, '2021-03-28 15:33:37', '2021-03-28 15:33:37', '2021-04-04 15:33:34');
INSERT INTO `oauth_access_tokens` VALUES ('95207d8e69d7bec5c2577b2d13cdb1953c69e7105b2bd595af4366532e92a6a6534c824b25ca23e5', 1, 1, 'Laravel', '[]', 0, '2021-04-11 16:01:41', '2021-04-11 16:01:41', '2021-04-18 16:01:38');
INSERT INTO `oauth_access_tokens` VALUES ('9889e3c06d2113e55e413781450eeb2a2ee7461c117d46872b29bd87c4ba40a915bd0cc1a96bc96e', 1, 1, 'Laravel', '[]', 0, '2021-03-26 02:30:07', '2021-03-26 02:30:07', '2021-04-02 02:30:04');
INSERT INTO `oauth_access_tokens` VALUES ('a50bf10c474bbd84ec5a1256f721ec132af7c2193e1dc251a72285f76b670081935face95ee2d656', 1, 1, 'Laravel', '[]', 0, '2021-04-10 15:39:56', '2021-04-10 15:39:56', '2021-04-17 15:39:55');
INSERT INTO `oauth_access_tokens` VALUES ('ad9e664be4ea12b5ce268ecd77c5e6bb19c25575f68cb08f52bbc464fb96d06b2a44ec90360ab824', 1, 1, 'Laravel', '[]', 0, '2021-04-14 04:29:48', '2021-04-14 04:29:48', '2021-04-21 04:29:46');
INSERT INTO `oauth_access_tokens` VALUES ('e325237890548d2d454f88e4db6f9fe602cc434bf220ad7771be677d3f2b2fc9a02bf1c255120f17', 1, 1, 'Laravel', '[]', 0, '2021-04-02 04:58:16', '2021-04-02 04:58:16', '2021-04-09 04:58:14');

-- ----------------------------
-- Table structure for oauth_auth_codes
-- ----------------------------
DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_auth_codes_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for oauth_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `provider` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `redirect` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_clients_user_id_index`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_clients
-- ----------------------------
INSERT INTO `oauth_clients` VALUES (1, NULL, 'Laravel Personal Access Client', 'OQzQeqdsvBnoBcNvXIWmkUZDVgIcGm7EkbnLLBkZ', NULL, 'http://localhost', 1, 0, 0, '2021-03-14 01:17:37', '2021-03-14 01:17:37');
INSERT INTO `oauth_clients` VALUES (2, NULL, 'Laravel Password Grant Client', 'whcxtpWYuVw2SaDGGA1zVfFQLUBcsa4SUvT88T6D', 'users', 'http://localhost', 0, 1, 0, '2021-03-14 01:17:37', '2021-03-14 01:17:37');

-- ----------------------------
-- Table structure for oauth_personal_access_clients
-- ----------------------------
DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of oauth_personal_access_clients
-- ----------------------------
INSERT INTO `oauth_personal_access_clients` VALUES (1, 1, '2021-03-14 01:17:37', '2021-03-14 01:17:37');

-- ----------------------------
-- Table structure for oauth_refresh_tokens
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens`  (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `oauth_refresh_tokens_access_token_id_index`(`access_token_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for page_connects
-- ----------------------------
DROP TABLE IF EXISTS `page_connects`;
CREATE TABLE `page_connects`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of page_connects
-- ----------------------------
INSERT INTO `page_connects` VALUES (1, 'Trang Đào tạo', NULL, 'http://daotao.ute.udn.vn/', '2021-04-20 23:41:37', '2021-04-20 23:41:39');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `comment_count` int(11) NULL DEFAULT NULL,
  `like_count` int(11) NULL DEFAULT NULL,
  `status` int(255) NULL DEFAULT NULL COMMENT '1. Hiện thị\r\n2. Ẩn\r\n',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `club_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (1, 1, 'DXD', '1', 'd-d', 0, 1, 1, '2021-04-11 09:50:21', '2021-04-11 09:50:23', 1);
INSERT INTO `posts` VALUES (2, 1, 'ABC', 'DXD`', 'a-b', 0, 1, 1, '2021-04-12 21:26:59', '2021-04-12 21:27:01', 1);

-- ----------------------------
-- Table structure for reply_comments
-- ----------------------------
DROP TABLE IF EXISTS `reply_comments`;
CREATE TABLE `reply_comments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'Admin', 'Admin Developer', '2021-03-12 08:06:49', '2021-03-12 08:06:49');
INSERT INTO `roles` VALUES (2, 'Club_manager', 'Club Manager', '2021-03-12 08:06:49', '2021-03-12 08:06:49');
INSERT INTO `roles` VALUES (3, 'User', 'Member Club', '2021-03-12 08:06:49', '2021-03-12 08:06:49');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `gender` tinyint(1) NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_otp_verification_enabled_at_login` tinyint(1) NOT NULL DEFAULT 0,
  `otp_type` enum('pin','mail','sms','google2fa') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pin',
  `otp` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `pin` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_google2fa_activated` tinyint(1) NOT NULL DEFAULT 0,
  `google2fa_secret` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_client_lock_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `clients_allowed` tinyint(4) NOT NULL DEFAULT 1,
  `is_ip_lock_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'DXD', 'admin1@email.com', '(380) 570-8417', NULL, NULL, NULL, NULL, '', '2021-03-14 01:17:35', 1, '$2y$10$nP2DDNTLuxCt2tnUlcO0xObIv.oe2ZJRqTeThbqpZsmgidv1MweYm', 0, 'pin', NULL, NULL, 0, NULL, 0, 1, 0, 1, 'sy1fK5QwkY', '2021-03-14 01:17:36', '2021-03-14 01:17:36');
INSERT INTO `users` VALUES (4, 'Rickey Will', 'beier.cayla@example.com', '1-571-224-6179 x4642', NULL, NULL, NULL, NULL, '', '2021-03-14 01:17:36', 3, '$2y$10$9OCNUjGymYpaLS9JDHn2CenNSwN/oaabxu5TAW/lvYV5v1q.46pXm', 0, 'pin', NULL, NULL, 0, NULL, 0, 1, 0, 1, 'BY783NWXrK', '2021-03-14 01:17:36', '2021-03-14 01:17:36');
INSERT INTO `users` VALUES (5, 'Sidney Tremblay DVM', 'holly.ledner@example.net', '1-761-382-6967 x9685', NULL, NULL, NULL, NULL, '', '2021-03-14 01:17:36', 3, '$2y$10$V2jZ.BSfveJnsKU.Sk2WzO0oO/BKZLolTkHh9Qzvi1dImih4WjYYW', 0, 'pin', NULL, NULL, 0, NULL, 0, 1, 0, 1, '9UgU2rJ1ph', '2021-03-14 01:17:36', '2021-03-14 01:17:36');
INSERT INTO `users` VALUES (6, 'Miles Gerhold', 'froob@example.net', '770.478.8573 x35749', NULL, NULL, NULL, NULL, '', '2021-03-14 01:17:36', 3, '$2y$10$oUFUex9YU9xTJwtugz2HUuoxpb..K3tH0nWHCg6/jqFVPwMRp0nqK', 0, 'pin', NULL, NULL, 0, NULL, 0, 1, 0, 1, 'KW1UREp1F6', '2021-03-14 01:17:36', '2021-03-14 01:17:36');
INSERT INTO `users` VALUES (7, 'Perry Cummings', 'minerva.stracke@example.net', '465-747-9872', NULL, NULL, NULL, NULL, '', '2021-03-14 01:17:36', 3, '$2y$10$DSBR8LyhrS7vw26gtiAGD.D568j0WesV5N5QFshvrpKJ1W6FVP9/C', 0, 'pin', NULL, NULL, 0, NULL, 0, 1, 0, 1, 'awIxk2JTZN', '2021-03-14 01:17:36', '2021-03-14 01:17:36');
INSERT INTO `users` VALUES (8, 'Miss Kailyn Emard PhD', 'elyssa.collier@example.com', '(281) 348-4984', NULL, NULL, NULL, NULL, '', '2021-03-14 01:17:36', 3, '$2y$10$.b61YCd6gD03WFFo05GQr.UT0zQu.6vODUErYt7tTYT4FH8BHmI/K', 0, 'pin', NULL, NULL, 0, NULL, 0, 1, 0, 1, 'BE06qBqN7l', '2021-03-14 01:17:36', '2021-03-14 01:17:36');
INSERT INTO `users` VALUES (12, 'abc1111', 'bianka.daniel@example.com', '1-505-804-5247 x01695', NULL, NULL, NULL, NULL, '', '2021-03-14 01:17:37', 3, '$2y$10$j67ejRUOw/K3W3IauhO/5Oic8k4OhrRAvKyCy2VeElVaQc/c3DaIu', 1, 'sms', NULL, NULL, 0, NULL, 0, 1, 0, 1, '3pUrUgitYY', '2021-03-14 01:17:37', '2021-04-19 02:12:12');

-- ----------------------------
-- Table structure for users_permissions
-- ----------------------------
DROP TABLE IF EXISTS `users_permissions`;
CREATE TABLE `users_permissions`  (
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
