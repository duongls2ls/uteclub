<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $dev_role = new Role();
        $dev_role->slug = 'Admin';
        $dev_role->name = 'Admin Developer';
        $dev_role->save();

        $manager_role = new Role();
        $manager_role->slug = 'Club_manager';
        $manager_role->name = 'Club Manager';
        $manager_role->save();
        //
        $manager_role = new Role();
        $manager_role->slug = 'user';
        $manager_role->name = 'Member Club';
        $manager_role->save();
    }
}
